
  <?php if(is_home()){?> 
  <!-- ROW GEN 2 -->
  <!-- [Widget Home Page 1] -->
  <div class="row-gen-2">
    <div class="row">
      <!-- SD 1 -->  
	      <div class="large-7 medium-7 small-12 column">
	      	<?php if ( is_active_sidebar( 'gentv1_slider_widget' ) ) : ?>
				<?php dynamic_sidebar( 'gentv1_slider_widget' ); ?>
			<?php endif; ?>
	       </div> 
      <!-- //SD 1 -->
      
      <!-- SD 2 --> 
      <div class="large-5 medium-5 small-12 column">
      		<?php if ( is_active_sidebar( 'gentv1_news_widget' ) ) : ?>
				<?php dynamic_sidebar( 'gentv1_news_widget' ); ?>
			<?php endif; ?>
      </div> 
      <!-- //SD 2 -->
      
    </div>
  </div>
  <!-- //ROW GEN 2 --> 

 <!-- //[Widget Home Page 1] -->


  <!-- ROW GEN 3 -->
  <div class="row-gen-3"> 
    <div class="row"> 
      <div class="large-12 columns">
      	<?php if ( is_active_sidebar( 'gentv1_top_widget' ) ) : ?>
				<?php dynamic_sidebar( 'gentv1_top_widget' ); ?>
			<?php endif; ?>
      </div>
    </div>    
  </div>
  <!-- //ROW GEN 3 -->
  
  
  <!-- ROW GEN 4 -->
  <div class="row-gen-3"> 
    <div class="row"> 
      <div class="large-12 columns">
      	<?php if ( is_active_sidebar( 'fourth_front_page_widget' ) ) : ?>
				<?php dynamic_sidebar( 'fourth_front_page_widget' ); ?>
			<?php endif; ?>
      </div>
    </div>    
  </div>
  <!-- //ROW GEN 4 -->

  <!-- ROW GEN 4 -->
  <div class="row-gen-3">
    <div class="row">
      <div class="large-12 columns">
        <hr>
      </div> 
      <div class="large-12 columns">
        <h2><small>RECENT WALPAPER</small></h2>
      </div>
      <div class="large-6 columns">
         <ul class="small-block-grid-4">
            <li>
              <a href="#" class="th"><img src="img/Alba-Garcia-Aguado-Salad-can-alter-620x413.jpg" alt="slide 2" /></a> 
            </li>
            <li>
              <a href="#" class="th"><img src="img/Alba-Garcia-Aguado-Salad-can-alter-620x413.jpg" alt="slide 2" /></a> 
            </li>
             <li>
              <a href="#" class="th"><img src="img/Alba-Garcia-Aguado-Salad-can-alter-620x413.jpg" alt="slide 2" /></a> 
            </li> 
             <li>
              <a href="#" class="th"><img src="img/Alba-Garcia-Aguado-Salad-can-alter-620x413.jpg" alt="slide 2" /></a> 
            </li> 
          </ul>
      </div> 
      <div class="large-6 columns">
        <div class="panel-ads"> 
            <img src="img/adsense728x90.gif" />
        </div> 
      </div>
      <!-- CONTENT -->
      <div class="large-12 columns">
        <ul class="large-block-grid-2 medium-block-grid-2 small-block-grid-1">
          <li>
            <div class="view view-first">
              <a href="#"><img src="img/iceland-620x400.jpg" alt="slide 3" /></a>
              <div class="mask">
                <div class="mask-content">
                  <h3><a href="#"><small>Amazing Scion Wallpaper Pictures</small></a></h3> 
                  <p>
                    Scion is a relatively new company (more or less owned by Toyota) that makes affordable yet stylish vehicles. One of their most popular models, the Scion TC, makes a great desktop background so we
                    <br><br><a href="#"><span class="label round">Read more</span></a>
                  </p>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="view view-first">
              <a href="#"><img src="img/iceland-620x400.jpg" alt="slide 3" /></a>
              <div class="mask">
                <div class="mask-content">
                  <h3><a href="#"><small>Amazing Scion Wallpaper Pictures</small></a></h3> 
                  <p>
                    Scion is a relatively new company (more or less owned by Toyota) that makes affordable yet stylish vehicles. One of their most popular models, the Scion TC, makes a great desktop background so we
                    <br><br><a href="#"><span class="label round">Read more</span></a>
                  </p>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="view view-first">
              <a href="#"><img src="img/iceland-620x400.jpg" alt="slide 3" /></a>
              <div class="mask">
                <div class="mask-content">
                  <h3><a href="#"><small>Amazing Scion Wallpaper Pictures</small></a></h3> 
                  <p>
                    Scion is a relatively new company (more or less owned by Toyota) that makes affordable yet stylish vehicles. One of their most popular models, the Scion TC, makes a great desktop background so we
                    <br><br><a href="#"><span class="label round">Read more</span></a>
                  </p>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="view view-first">
              <a href="#"><img src="img/iceland-620x400.jpg" alt="slide 3" /></a>
              <div class="mask">
                <div class="mask-content">
                  <h3><a href="#"><small>Amazing Scion Wallpaper Pictures</small></a></h3> 
                  <p>
                    Scion is a relatively new company (more or less owned by Toyota) that makes affordable yet stylish vehicles. One of their most popular models, the Scion TC, makes a great desktop background so we
                    <br><br><a href="#"><span class="label round">Read more</span></a>
                  </p>
                </div>
              </div>
            </div>
          </li> 
          <li>
            <div class="view view-first">
              <a href="#"><img src="img/iceland-620x400.jpg" alt="slide 3" /></a>
              <div class="mask">
                <div class="mask-content">
                  <h3><a href="#"><small>Amazing Scion Wallpaper Pictures</small></a></h3> 
                  <p>
                    Scion is a relatively new company (more or less owned by Toyota) that makes affordable yet stylish vehicles. One of their most popular models, the Scion TC, makes a great desktop background so we
                    <br><br><a href="#"><span class="label round">Read more</span></a>
                  </p>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="view view-first">
              <a href="#"><img src="img/iceland-620x400.jpg" alt="slide 3" /></a>
              <div class="mask">
                <div class="mask-content">
                  <h3><a href="#"><small>Amazing Scion Wallpaper Pictures</small></a></h3> 
                  <p>
                    Scion is a relatively new company (more or less owned by Toyota) that makes affordable yet stylish vehicles. One of their most popular models, the Scion TC, makes a great desktop background so we
                    <br><br><a href="#"><span class="label round">Read more</span></a>
                  </p>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <!-- CONTENT -->  
    </div>
  </div>
  <!-- //ROW GEN 4 -->
  
  </div>
  </div>
  <!-- //WIDE -->
  
  <?php } ?> 
  
