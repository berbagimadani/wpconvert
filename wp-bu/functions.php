<?php
/**
 * Gen Themes functions and definitions.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
require_once ( get_stylesheet_directory() . '/functions/cigar_shortcode.php');
require_once ( get_stylesheet_directory() . '/functions/foundation4-topbar-menu.php'); 

if( file_exists( get_template_directory().'/class/foundation4-topbar-walker.php' ) ) {
	require_once ( get_stylesheet_directory() . '/class/foundation4-topbar-walker.php'); 
}
  
if ( ! isset( $content_width ) )
	$content_width = 604;
	$root = get_template_directory_uri(); 
 
function genthemesv1_scripts_styles() {
	global $root;
	wp_enqueue_style( 'foundation', $root . '/assets/css/foundation.css', array(), '1.0.0' );
	wp_enqueue_style( 'styles', $root . '/assets/css/styles.css', array(), '1.0.0' );
	wp_enqueue_style( 'menu', $root . '/assets/css/menu.css', array(), '1.0.0' );
	wp_enqueue_style( 'hover', $root . '/assets/css/hover.css', array(), '1.0.0' );
	wp_enqueue_style( 'tab', $root . '/assets/css/tab.css', array(), '1.0.0' );
	wp_enqueue_style( 'font-awesome', $root . '/assets/css/font-awesome.css', array(), '1.0.0' );
	wp_enqueue_style( 'Playfair+Display+SC', 'http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700italic,700' );
	wp_enqueue_style( 'Droid+Serif', 'http://fonts.googleapis.com/css?family=Droid+Serif:400,700');
	wp_enqueue_style( 'Duru+Sans', 'http://fonts.googleapis.com/css?family=Duru+Sans' );
	
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'modernizr', $root. "/assets/js/modernizr.js", array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'foundation.min', $root. "/assets/js/foundation.min.js", array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'foundation.min', $root. "/assets/js/foundation/foundation.orbit.js", array('jquery'), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'genthemesv1_scripts_styles' );
 
function genthemesv1_setup() { 
	
	require( get_template_directory() . '/widgets/GenTV1_Slider_Widget.php' );
	require( get_template_directory() . '/widgets/GenTV1_News_Widget.php' );
	require( get_template_directory() . '/widgets/GenTV1_Top_Widget.php' );
	
	load_theme_textdomain( 'genthemesv1', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', 'genthemesv1' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'genthemesv1_setup' );

function genthemesv1_widgets_init() {
	
	register_widget('GenTV1_Slider_Widget');
	register_widget('GenTV1_News_Widget');
	register_widget('GenTV1_Top_Widget');
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'genthemesv1' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'GenTV1 Slider Widget', 'genthemesv1' ),
		'id' => 'gentv1_slider_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'GenTV1 News Widget', 'genthemesv1' ),
		'id' => 'gentv1_news_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'GenTV1 Top Widget', 'genthemesv1' ),
		'id' => 'gentv1_top_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
}
add_action( 'widgets_init', 'genthemesv1_widgets_init' ); 