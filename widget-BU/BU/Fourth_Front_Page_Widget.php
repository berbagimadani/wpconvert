<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Fourth_Front_Page_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Fourth_Front_Page_Widget';
	public $namewidget = 'Fourth_Front_Page_Widget';
	public $titlewidget = 'Fourth_Front_Page_Widget';
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function Fourth_Front_Page_Widget() {
		$widget_ops = array( 'classname' => $this->slugwidget, 'description' => __( 'Use this widget to list your recent Aside, Status, Quote, and Link posts', 'genthemesv1' ) );
		$this->WP_Widget( $this->slugwidget, __($this->namewidget, $this->versionwidget ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP );
		 
		
		$image_link1 = apply_filters( 'widget_image_link1', empty($instance['image_link1'] ) ? __('', $this->versionwidget ) : $instance['image_link1'], $instance, $this->id_base);
		$image_link2 = apply_filters( 'widget_image_link2', empty($instance['image_link2'] ) ? __('', $this->versionwidget ) : $instance['image_link2'], $instance, $this->id_base);
		$image_link3 = apply_filters( 'widget_image_link3', empty($instance['image_link3'] ) ? __('', $this->versionwidget ) : $instance['image_link3'], $instance, $this->id_base);
		
		$title = apply_filters( 'widget_title', empty($instance['title'] ) ? __('', $this->versionwidget ) : $instance['title'], $instance, $this->id_base);
		$description = apply_filters( 'widget_description', empty($instance['description'] ) ? __('', $this->versionwidget ) : $instance['description'], $instance, $this->id_base);
		
		if ( ! isset($instance['number'] ) )$instance['number'] = '7'; 
		if ( ! $number = absint($instance['number'] ) )
		$number = 7;
		if ( ! isset($instance['number2'] ) )$instance['number2'] = '12'; 
		if ( ! $number2 = absint($instance['number2'] ) )
		$number2 = 12; 
 			  ?> 
				 
      		
      		<div class="sixteen columns"> 
			<h1><?php echo html_entity_decode($title); ?></h1> 
			</div>  
			<div class="sixteen columns"> 
			<h1><?php echo $number; ?></h1> 
			</div>
			<div class="sixteen columns"> 
			<h1><?php echo $number2; ?></h1> 
			</div>   
            <div class="one-third column"> 
            	<img src="<?php echo $image_link1; ?>">
            </div>
            <div class="one-third column">
            	<img src="<?php echo $image_link2; ?>">
            </div>
            <div class="one-third column">
            	<img src="<?php echo $image_link3; ?>">
            </div>
            
            <div class="twelve columns offset-by-two">
            	<div class="introtext">
					<?php echo html_entity_decode($description); ?>	
				</div>
            </div> 
      
      
 			<?php

			echo $after_widget;

			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata();

			// end check for ephemeral posts
		 

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;  
		
		$instance['image_link1'] = strip_tags( $new_instance['image_link1']);
		$instance['image_link2'] = strip_tags( $new_instance['image_link2']);
		$instance['image_link3'] = strip_tags( $new_instance['image_link3']);
		
		$instance['title'] = esc_html( $new_instance['title']);
		$instance['description'] = esc_html( $new_instance['description']);
		
		$instance['number'] = (int) $new_instance['number'];
		$instance['number2'] = (int) $new_instance['number2'];
	
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {  
		
		$title = isset( $instance['title']) ? esc_html( $instance['title'] ) : '';
		$number = isset( $instance['number']) ? absint( $instance['number'] ) : 7;
		$number2 = isset( $instance['number2']) ? absint( $instance['number2'] ) : 12;
		$image_link1 = isset( $instance['image_link1']) ? esc_attr( $instance['image_link1'] ) : '';
		$image_link2 = isset( $instance['image_link2']) ? esc_attr( $instance['image_link2'] ) : '';
		$image_link3 = isset( $instance['image_link3']) ? esc_attr( $instance['image_link3'] ) : '';
		$description = isset( $instance['description']) ? esc_html( $instance['description'] ) : '';
	?>
		 
		
		<p><label for="<?php echo esc_attr($this->get_field_id( 'title' ) ); ?>"><?php _e( 'title:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr($title); ?>"/></p>

		<p><label for="<?php echo esc_attr($this->get_field_id( 'number' ) ); ?>"><?php _e( 'number:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr($number); ?>"/></p>

		<p><label for="<?php echo esc_attr($this->get_field_id( 'number2' ) ); ?>"><?php _e( 'number2:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'number2' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'number2' ) ); ?>" type="text" value="<?php echo esc_attr($number2); ?>"/></p>

		<p><label for="<?php echo esc_attr($this->get_field_id( 'image_link1' ) ); ?>"><?php _e( 'image_link1:', $this->versionwidget ); ?></label></p>
		<input type="text" class="widefat image_link1" id="<?php echo esc_attr($this->get_field_id( 'image_link1' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'image_link1' ) ); ?>" value="<?php echo esc_attr($image_link1); ?>"/>	
			<div class="image_link1">
			<?php 
			if(!empty($image_link1)){
			echo '<img src="'.$image_link1.'" style="width:100%">';
			}
			?>
			</div>
			<input type="button" id="select-image_link1" class="upload_image_button button" value="Select Image" name="image_link1">

		<p><label for="<?php echo esc_attr($this->get_field_id( 'image_link2' ) ); ?>"><?php _e( 'image_link2:', $this->versionwidget ); ?></label></p>
		<input type="text" class="widefat image_link2" id="<?php echo esc_attr($this->get_field_id( 'image_link2' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'image_link2' ) ); ?>" value="<?php echo esc_attr($image_link2); ?>"/>	
			<div class="image_link2">
			<?php 
			if(!empty($image_link2)){
			echo '<img src="'.$image_link2.'" style="width:100%">';
			}
			?>
			</div>
			<input type="button" id="select-image_link2" class="upload_image_button button" value="Select Image" name="image_link2">

		<p><label for="<?php echo esc_attr($this->get_field_id( 'image_link3' ) ); ?>"><?php _e( 'image_link3:', $this->versionwidget ); ?></label></p>
		<input type="text" class="widefat image_link3" id="<?php echo esc_attr($this->get_field_id( 'image_link3' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'image_link3' ) ); ?>" value="<?php echo esc_attr($image_link3); ?>"/>	
			<div class="image_link3">
			<?php 
			if(!empty($image_link3)){
			echo '<img src="'.$image_link3.'" style="width:100%">';
			}
			?>
			</div>
			<input type="button" id="select-image_link3" class="upload_image_button button" value="Select Image" name="image_link3">

		<p><label for="<?php echo esc_attr($this->get_field_id( 'description' ) ); ?>"><?php _e( 'description:', $this->versionwidget ); ?></label></p>
		<p><textarea rows="8" cols="20" class="widefat" id="<?php echo esc_attr($this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'description' ) ); ?>"><?php echo esc_attr($description); ?></textarea></p>

		 
	<?php
	}
}

// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Fourth_Front_Page_Widget");') );

function my_admin_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'my_admin_scripts');

?>