<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class GenTV1_Html_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'GenTV1_Html_Widget';
	public $namewidget = 'GenTV1_Html_Widget';
	public $titlewidget = 'GenTV1_Html_Widget';
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function GenTV1_Html_Widget() {
		$widget_ops = array( 'classname' => $this->slugwidget, 'description' => __( 'Use this widget to list your recent Aside, Status, Quote, and Link posts', 'genthemesv1' ) );
		$this->WP_Widget( $this->slugwidget, __($this->namewidget, $this->versionwidget ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP );
	
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( $this->titlewidget, $this->versionwidget ) : $instance['title'], $instance, $this->id_base);
		$tag_html_input = apply_filters( 'widget_html_input', empty( $instance['tag_html_input'] ) ? __( $this->titlewidget, $this->versionwidget ) : $instance['tag_html_input'], $instance, $this->id_base);
 		
		 
 			  ?> 
				 
      	<h2><small><?php echo $title; ?></small></h2> 
        <?php echo html_entity_decode($tag_html_input);?>
      
 			<?php

			echo $after_widget;

			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata();

			// end check for ephemeral posts
		 

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['tag_html_input'] = esc_html( $new_instance['tag_html_input'] ); 
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {
		$title = isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$tag_html_input = isset( $instance['tag_html_input']) ? esc_html( $instance['tag_html_input'] ) : '';  
?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', $this->versionwidget ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
			
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'tag_html_input' ) ); ?>"><?php _e( 'Html Code:', $this->versionwidget ); ?></label>
			<textarea rows="8" cols="20" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tag_html_input' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tag_html_input' ) ); ?>"><?php echo esc_html( $tag_html_input ); ?></textarea> 
			 
		<?php
	}
}