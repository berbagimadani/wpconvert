<?php 
class FolderName
{
	static public function set($name, $value)
    {
        $GLOBALS[$name] = $value;
    }
    static public function get($name)
    {
        return $GLOBALS[$name];
    }
}
$homepage = file_get_contents('theme/index.html'); 
get_content2('display',$homepage);
function get_content2( $tag , $content )
{
	/* get css styles in function */
	$tag_wg ="function"; 
    preg_match_all("/{".$tag_wg."[^}]*}(.*?){\/$tag_wg}/si", $content, $matches_wg);
	foreach ($matches_wg[1] as $key => $value) {  
     $dis_function .= $value; 
    } 
     
    
    $tag_name_fun = "function";
    preg_match_all("/{".$tag_wg."\:*(.*?)}/si", $dis_function, $matches_name_fun);
	foreach ($matches_name_fun[1] as $key => $value_name_fun) {  
		$exp_variable = explode(";", $value_name_fun); 
		$name_fun .= $exp_variable[1]; 
		if (!file_exists($name_fun."/widgets")) {
	    	mkdir($name_fun."/widgets", 0777, true);
	    	mkdir($name_fun."/theme-options", 0777, true);
		} 
		recurse_copy('templates/js', 'js',$name_fun);
		recurse_copy('templates/functions', 'functions',$name_fun);
		recurse_copy('templates/class', 'class',$name_fun);
		recurse_copy('templates/inc', 'inc',$name_fun);
		recurse_copy('templates/theme-options', 'theme-options',$name_fun);
		
		recurse_copy('templates/theme-options/assets', 'theme-options/assets',$name_fun);
		recurse_copy('templates/theme-options/assets/css', 'theme-options/assets/css',$name_fun);
		recurse_copy('templates/theme-options/assets/js', 'theme-options/assets/js',$name_fun);
		recurse_copy('templates/theme-options/assets/img', 'theme-options/assets/img',$name_fun);
		recurse_copy('templates/theme-options/assets/img/pattern', 'theme-options/assets/img/pattern',$name_fun);
		
		return FolderName::set('themeFolder',$name_fun);
    }
}

function recurse_copy($src,$dst,$name_fun) {
    $dir = opendir($src);
    @mkdir($name_fun.'/'.$dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                //recurse_copy($src . '/' .$file,$name_fun.'/'.$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$name_fun.'/'.$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function copyfolder($source, $destination,$name_fun) 
{ 
       $directory = opendir($source); 
 
       mkdir($name_fun.'/'.$destination); 

       while(($file = readdir($directory)) != false) 
       { 
 		copy($source.'/' .$file, $name_fun.'/'.$destination.'/'.$file);  
       } 

} 

?>