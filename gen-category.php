<?php 
include("ClassFolderName.php"); 
?>
<?php
$homepage = file_get_contents('theme/category.html');


get_content('display',$homepage);

function get_content($tag , $content){
	$tag_com ="gt:category"; 
    preg_match_all("/<".$tag_com."[^>]*>(.*?)<\/$tag_com>/si", $content, $matches_com);
     
	foreach ($matches_com[1] as $key => $value_com) {  
     	$vals_com .= $value_com;  
    }
   	 
	$tag = "query";
    preg_match_all("/{".$tag.":\"*(.*?)\}/si", $vals_com, $matches);
    $str = $matches[1];
	foreach ($str as $key => $value_s) {
		 if( $value_s == "category"){
		 	$s .= $value_s;
		 	$q .= '<?php  
					$paged = (get_query_var(\'paged\')) ? get_query_var(\'paged\') : 1;
					$args = array(
					    \'post_type\' => \'post\',
						\'category_name\' =>  $catName->slug,
					    \'paged\'=> $paged,
						\'order\' => \'DESC\', 
					);  
					$the_query = new WP_Query( $args ); 
						while ($the_query->have_posts()):
						$the_query->the_post();   
						$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), \'single-post-thumbnail\' );
						$featured_image = $featured_image_array[0];
					?>';
		 }
	}
	$search = array('{get_header}',
	'{is_category}',   
    '{close_is_category}',
    '{breadcrumbs}',
    '{cat_name}',
	"{query:$s}",
	"{/query:$s}",
	'{post_title}',  
	'{post_author}',
	'{post_date}',  
	'{count_comment}',
	'{count_view}',
	'{pager}',
	'{permalink}',
    
    '{get_sidebar}',
    '{get_footer}'
    );
    
    $replace = array('<?php get_header(); ?>',
    '<?php if (is_category( )) {?>',
    '<?php } ?>',
    '<?php 
				if (!is_home()) { 
					if (is_category() || is_single()) {
						echo  the_category(\' \'); 
					}	 
				} ?>',
	'
				<?php 
					$cat = get_query_var(\'cat\');
					$catName = get_category ($cat);
					echo $catName->cat_name;
				?>',
    "$q",
    '<?php endwhile;?>',
    '<?php echo get_the_title();?>',
    '<?php echo get_the_author_meta(\'nickname\');?>',
    '<?php echo get_the_modified_date()?>', 
    "<?php comments_popup_link( '0 Comments', '1 Comments', '% Comments', '', '0 Comments'); ?>",
	'<?php echo view_count(get_the_ID())?>',
    "
				<?php 
					\$big = 999999999; // need an unlikely integer 
					echo paginate_links( array(
						'base' => str_replace( \$big, '%#%', esc_url( get_pagenum_link( \$big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => \$the_query->max_num_pages
					) ); 
				?>
	",
    '<?php echo get_permalink();?>',
    
    '<?php get_sidebar(); ?>',
    '<?php get_footer(); ?>'
    );
    
    $content = str_replace($search,$replace,$vals_com);
    
    $tag_img = "img_query";
	    preg_match_all("/{".$tag_img.":\"*(.*?)\}/si", $content, $matches_img);
	    $str = $matches_img[1];
		foreach ($str as $key => $value_s) {
			$exp_variable = explode(",", $value_s); 
			$img_query .= "{img_query:$value_s};genthemes";
			 
			if(is_numeric($exp_variable[0])){ 
				$img_query_var .='<?php 
								$default_attr = array( 
								\'class\'	=> "'.$exp_variable[2].'",
								\'alt\'	=> trim(strip_tags(get_the_excerpt())),
								\'title\'	=> trim(strip_tags( get_the_title())),
								);
								?>
								<?php 
								if ( has_post_thumbnail()) {
								   echo get_the_post_thumbnail(get_the_ID(), array('.$exp_variable[0].','.$exp_variable[1].'), $default_attr); 
								}
								 else{
								 	echo \'<img src="\'.opt_genthemes(\'thumbnail\').\'">\';
								 }
							?>;genthemes';
			}
			else{ 
				$img_query_var .='<?php 
								$default_attr = array( 
								\'class\'	=> "'.$exp_variable[1].'",
								\'alt\'	=> trim(strip_tags(get_the_excerpt())),
								\'title\'	=> trim(strip_tags( get_the_title())),
								);
								?>
								<?php 
								if ( has_post_thumbnail()) {
								   echo get_the_post_thumbnail(get_the_ID(), \''.$exp_variable[0].'\', $default_attr); 
								}
								 else{
								 	echo \'<img src="\'.opt_genthemes(\'thumbnail\').\'">\';
								 }
							?>;genthemes';
			}  
		} 
		
	$exp_variable = explode(";genthemes", $img_query);
	$exp_variable_2 = explode(";genthemes", $img_query_var);
	
	$search_m2 = $exp_variable;
	$replace_m2 = $exp_variable_2;
	$m_content2 = str_replace($search_m2, $replace_m2, $content);
	
	
	$tag_content = "post_content";
	    preg_match_all("/{".$tag_content.":\"*(.*?)\}/si", $m_content2, $matches_content);
	    $str = $matches_content[1];
		foreach ($str as $key => $value_s) { 
			$post_content .= "{post_content:$value_s};genthemes";
			 
			if(is_numeric($value_s)){ 
				$post_content_var .='<?php echo wp_trim_words( get_the_content(),'.$value_s.' );?>;genthemes';
			}
			else{ 
				$post_content_var .='<?php echo the_content(); ?>;genthemes';
			}  
		} 
		
	$exp_variable_content = explode(";genthemes", $post_content);
	$exp_variable_content_2 = explode(";genthemes", $post_content_var);
	
	$search_m3 = $exp_variable_content;
	$replace_m3 = $exp_variable_content_2;
	$m_content3 = str_replace($search_m3, $replace_m3, $m_content2);
      
    $file_sidebar= FolderName::get('themeFolder')."/category.php";
    writefile($file_sidebar,$m_content3);
     
}

function strSlug($val){  
	$exp_variable = str_replace(" ","-",$val); 
	return strtolower($exp_variable);
}
function replaceString($val){ 
	$exp_variable = explode(":", $val); 
	return $exp_variable[0];
}
function writefile($file,$content){ 
	$ourFileHandle = fopen($file, 'w') or die("can't open file");
	fwrite($ourFileHandle,$content);
	fclose($ourFileHandle);
}
 
	
?>