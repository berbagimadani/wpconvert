/*jQuery(document).ready(function() {
jQuery('#upload_image_button_schedule').click(function() {
 formfield = jQuery('#schedule_logo_vs').attr('name');
 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
 return false;
});
 
window.send_to_editor = function(html) {
 imgurl = jQuery('img',html).attr('src');
 jQuery("#schedule_logo_vs_preview").html('<img src="' + imgurl + '" />');
 jQuery("#preview").hide();
 jQuery('#schedule_logo_vs').val(imgurl); 
 tb_remove();
}
 
});*/
/*
 * Attaches the image uploader to the input field
 */
jQuery(document).ready(function(){

	// Instantiates the variable that holds the media library frame.
	var meta_image_frame;

	// Runs when the image button is clicked.
	jQuery('#click-favicon-image').click(function(e){

		// Prevents the default action from occuring.
		e.preventDefault();

		// If the frame already exists, re-open it.
		if ( meta_image_frame ) {
			meta_image_frame.open();
			return;
		}

		// Sets up the media library frame
		meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
			title: favicon_image.title,
			button: { text:  favicon_image.button },
			library: { type: 'image' }
		});

		// Runs when an image is selected.
		meta_image_frame.on('select', function(){

			// Grabs the attachment selection and creates a JSON representation of the model.
			var media_attachment = meta_image_frame.state().get('selection').first().toJSON();

			// Sends the attachment URL to our custom image input field.
			jQuery('#favicon-image').val(media_attachment.url);
			jQuery("#favicon_preview").html('<img src="' + media_attachment.url + '" />');
			jQuery("#preview").hide();
		});

		// Opens the media library frame.
		meta_image_frame.open();
	});

	/* 22222222 */
	
	var meta_image_frame_2;

	// Runs when the image button is clicked.
	jQuery('#click-logo-image').click(function(e){

		// Prevents the default action from occuring.
		e.preventDefault();

		// If the frame already exists, re-open it.
		if ( meta_image_frame_2 ) {
			meta_image_frame_2.open();
			return;
		}

		// Sets up the media library frame
		meta_image_frame_2 = wp.media.frames.meta_image_frame_2 = wp.media({
			title: favicon_image.title,
			button: { text:  favicon_image.button },
			library: { type: 'image' }
		});

		// Runs when an image is selected.
		meta_image_frame_2.on('select', function(){

			// Grabs the attachment selection and creates a JSON representation of the model.
			var media_attachment_2 = meta_image_frame_2.state().get('selection').first().toJSON();

			// Sends the attachment URL to our custom image input field.
			jQuery('#logo-image').val(media_attachment_2.url);
			jQuery("#logo_preview").html('<img src="' + media_attachment_2.url + '" />');
			jQuery("#preview-logo").hide();
		});

		// Opens the media library frame.
		meta_image_frame_2.open();
	});
	
	/* 333333333 */
	
	var meta_image_frame_3;

	// Runs when the image button is clicked.
	jQuery('#click-bgimage').click(function(e){

		// Prevents the default action from occuring.
		e.preventDefault();

		// If the frame already exists, re-open it.
		if ( meta_image_frame_3 ) {
			meta_image_frame_3.open();
			return;
		}

		// Sets up the media library frame
		meta_image_frame_3 = wp.media.frames.meta_image_frame_3 = wp.media({
			title: favicon_image.title,
			button: { text:  favicon_image.button },
			library: { type: 'image' }
		});

		// Runs when an image is selected.
		meta_image_frame_3.on('select', function(){

			// Grabs the attachment selection and creates a JSON representation of the model.
			var media_attachment_3 = meta_image_frame_3.state().get('selection').first().toJSON();

			// Sends the attachment URL to our custom image input field.
			jQuery('#bgimage').val(media_attachment_3.url);
			jQuery("#bgimage_preview").html('<img src="' + media_attachment_3.url + '" />');
			jQuery("#preview-bgimage").hide();
		});

		// Opens the media library frame.
		meta_image_frame_3.open();
	});
});
