<?php
define('GENTHEMES_SHORTNAME', 'genthemesfr');  
define('GENTHEMES_PAGE_BASENAME', 'genthemesfr-settings');
/*
 * Specify Hooks/Filters
 */
add_action( 'admin_menu', 'genthemesfr_add_menu' );  
add_action( 'admin_init', 'genthemesfr_register_settings' );
 
 /**
 * Helper function for defining variables for the current page
 *
 * @return array
 */
if ( is_admin() ) :
function genthemesfr_get_settings() { 
	$output = array(); 
	// put together the output array
	$output['genthemesfr_option_name']			= 'genthemesfr_options';  
	$output['genthemesfr_option_theme_name'] 	= 'genthemesfr_options_theme'; 
	$output['genthemesfr_page_title']			= 'GenThemes Framework Settings Page'; 
	return $output;
}

/*
 * Register our setting
 */
function genthemesfr_register_settings(){  
	$settings_output = genthemesfr_get_settings();
	$genthemesfr_option_name = $settings_output['genthemesfr_option_name'];
	$genthemesfr_option_theme_name = $settings_output['genthemesfr_option_theme_name'];
	
	//register_setting( $option_group, $option_name, $sanitize_callback );
	register_setting($genthemesfr_option_name, $genthemesfr_option_name, 'genthemesfr_validate_options' );
	 
}
function genthemesfr_net_settings_scripts(){
	$settings_output = genthemesfr_get_settings();
	$genthemesfr_option_name = $settings_output['genthemesfr_option_name'];
	 
	if ( 'appearance_page_genthemesfr-settings' == get_current_screen() -> id ) {
		wp_enqueue_script('jquery');
		wp_enqueue_script('media-upload');
		wp_enqueue_style('thickbox');
		wp_enqueue_script('thickbox');
		wp_register_script('my-upload', get_template_directory_uri().'/theme-options/assets/js/media_upload.js', array('jquery','media-upload','thickbox'));
		wp_enqueue_script('my-upload'); 
		
		wp_enqueue_style('genthemesfr_net_foundation_css', get_template_directory_uri().'/theme-options/assets/css/foundation.css');
    	wp_enqueue_style('genthemesfr_net_styles_css', get_template_directory_uri() .'/theme-options/assets/css/styles.css');
    	wp_enqueue_style('genthemesfr_net_opensans_css', 'http://fonts.googleapis.com/css?family=Open+Sans:400,700');    
		
    	wp_enqueue_script( 'genthemesv1-theme-options', get_template_directory_uri() . '/theme-options/theme-options.js', array( 'farbtastic' ), '2011-06-10' );
		wp_enqueue_style( 'farbtastic' );
	}
}
add_action('admin_print_styles-appearance_page_genthemesfr-settings', 'genthemesfr_net_settings_scripts');

function genthemesfr_add_menu(){ 
	$genthemesfr_settings_page = add_theme_page('Genthemes Options','GenthemesOptions','manage_options', GENTHEMES_PAGE_BASENAME, 'genthemesfr_settings_page_fn');	 
}
function genthemesfr_settings_page_fn() {
	$root = get_template_directory_uri();
	$folder = './images/';
	$settings_output = genthemesfr_get_settings();
	$genthemesfr_option_name = $settings_output['genthemesfr_option_name'];
	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false; // This checks whether the form has just been submitted. ?>
  
	<div class="row left">
    <div class="large-12 columns">
      <div class="panel panel-custome-1">

        <div class="row">
          <div class="large-12 columns">
          	<div class="panel callout-custome radius">
    	        <h5>GENTHEMES OPTIONS PANEL </h5> 
          	</div>
          </div>
        </div> 
         
        
        <div class="row">
          <div class="large-12 columns">    
             
            <?php $options = get_option($genthemesfr_option_name); ?>
            
            <form method="post" action="options.php">
           	<?php settings_fields( $genthemesfr_option_name );?>
                 <a href="#" class="label tiny radius 20 success pad-bottom">General</a>
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Favicon</label>
                    </div>
                    <div class="large-12 columns">
                      <div class="row collapse">
                        <div class="small-10 columns">
                          <input type="text" class="<?php echo $genthemesfr_option_name;?>_favicon" id="<?php echo $genthemesfr_option_name;?>_favicon" name="<?php echo $genthemesfr_option_name;?>[favicon]" value="<?php if(isset($options['favicon'])) { echo $options['favicon']; }?>">
 			 			  <input type="button" id="select-<?php echo $genthemesfr_option_name;?>_favicon" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_favicon">
 			 			  <?php if(!empty($options['favicon'])) { ?>
 			 			  <input type="button" id="select-<?php echo $genthemesfr_option_name;?>_favicon" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_favicon">
 			 			  <?php }?>
 			 			  <div class="<?php echo $genthemesfr_option_name;?>_favicon" style="padding-top:10px"> 
                        	<?php if(isset($options['favicon'])) { echo "<img src='".$options['favicon']."' style='width:10%'>"; }?>
                      	  </div> 
 			 			</div> 
                      </div>
                      
                      <hr>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Logo</label>
                    </div>
                    <div class="large-12 columns">
                      <div class="row collapse">
                        <div class="small-10 columns">
                          <input type="text" class="<?php echo $genthemesfr_option_name;?>_logo" id="<?php echo $genthemesfr_option_name;?>_logo" name="<?php echo $genthemesfr_option_name;?>[logo]" value="<?php if(isset($options['logo'])) { echo $options['logo']; }?>">
 			 			  <input type="button" id="select-<?php echo $genthemesfr_option_name;?>_logo" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_logo">
 			 			  <input type="button" id="select-<?php echo $genthemesfr_option_name;?>_logo" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_logo">
 			 			  <div class="<?php echo $genthemesfr_option_name;?>_logo" style="padding-top:10px"> 
                        	<?php if(isset($options['logo'])) { echo "<img src='".$options['logo']."' style='width:10%'>"; }?>
                      	  </div> 
                        </div> 
                      </div> 
                      <hr>
                    </div>
                  </div> 
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Descriptions</label>
                      <input type="text" placeholder="Enter your meta description" name="<?php echo $genthemesfr_option_name;?>[description]" value="<?php if(isset($options['description'])) { echo esc_attr($options['description']); }?>"/>
                    </div>
                    <div class="large-12 columns">
                      <label>Keywords</label>
                      <input type="text" placeholder="Enter your meta keyword" name="<?php echo $genthemesfr_option_name;?>[keyword]" value="<?php if(isset($options['keyword'])) { echo esc_attr($options['keyword']); }?>"/>
                    </div>
                    <div class="large-12 columns"> 
                        <label>Layout Web</label> 
                        <select name="<?php echo $genthemesfr_option_name;?>[layout_web]">
                            <option value="row-gen-wide" <?php if(isset($options['layout_web'])) { echo selected($options['layout_web'],'row-gen-wide'); }?>>Full Width</option>
                            <option value="row-gen-boxed" <?php if(isset($options['layout_web'])) { echo selected($options['layout_web'],'row-gen-boxed'); }?>>Boxed</option> 
                        </select> 
                    </div>
                    <div class="large-12 columns"> 
                        <label>Thumbnail</label> 
                        <select name="<?php echo $genthemesfr_option_name;?>[thumbnail]">
                        	<option>--Select Thumbnail--</option>
                            <option value="<?php echo $root."/assets/img/thumbnail.jpg"; ?>" <?php if(isset($options['thumbnail'])) { echo selected($options['thumbnail'],$root."/assets/img/thumbnail.jpg"); }?>>Style 1</option>
                            <option value="<?php echo $root."/assets/img/logo.png"; ?>" <?php if(isset($options['thumbnail'])) { echo selected($options['thumbnail'],$root."/assets/img/logo.png"); }?>>Style 2</option> 
                        </select> 
                    </div> 
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Google Analytic Code</label>
                      <textarea placeholder="Google analytic code" class="custome-textarea" name="<?php echo $genthemesfr_option_name;?>[google-analytic]"><?php if(isset($options['google-analytic'])) { echo esc_attr($options['google-analytic']); }?></textarea>
                    </div>
                  </div>
                  
                  
                  <label for="checkbox1"><a href="#" class="label tiny radius 20 success pad-bottom">Custome Background</a></label>
                  
                  <div class="row">
                    <div class="large-12 columns">
                    <div class="panel">
	                  <div class="row">
	                    <div class="large-12 columns">
	                      <input type="radio" name="<?php echo $genthemesfr_option_name;?>[bgradio]" value="bgimage" id="bgimage" <?php if(isset($options['bgradio'])) { checked( $options['bgradio'], 'bgimage'); }?>>
	                      <label for="bgimage">Background Image</label> 
	                    </div>
	                    <div class="large-12 columns">
	                      <div class="row collapse">
	                        <div class="small-10 columns"> 
	                          	<input type="text" class="<?php echo $genthemesfr_option_name;?>_bgimage" id="<?php echo $genthemesfr_option_name;?>_bgimage" name="<?php echo $genthemesfr_option_name;?>[bgimage]" value="<?php if(isset($options['bgimage'])) { echo $options['bgimage']; }?>">
	 			 			 	<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_bgimage" class="upload_image_button button" value="Select Image" name="<?php echo $genthemesfr_option_name;?>_bgimage">
	 			 			  	<input type="button" id="select-<?php echo $genthemesfr_option_name;?>_bgimage" class="remove_image_button button" value="Remove Image" name="<?php echo $genthemesfr_option_name;?>_bgimage">
	 			 			  	<div class="<?php echo $genthemesfr_option_name;?>_bgimage" style="padding-top:10px"> 
	                        	<?php if(isset($options['bgimage'])) { echo "<img src='".$options['bgimage']."' style='width:10%'>"; }?>
	                      	  	</div> 
	                        </div> 
	                      </div> 
	                    </div>
	                    <div class="large-12 columns">
	                    <fieldset>
    					<legend>Attribute Background Image</legend> 
	                      <input id="checkbox1" type="radio" name="<?php echo $genthemesfr_option_name;?>[attr_bgimage]" value="fixed" <?php if(isset($options['attr_bgimage'])) { checked( $options['attr_bgimage'], 'fixed'); } ?>><label for="checkbox1">Full Screen Fixed</label>
	                      <input id="checkbox2" type="radio" name="<?php echo $genthemesfr_option_name;?>[attr_bgimage]" value="relative" <?php if(isset($options['attr_bgimage'])) { checked( $options['attr_bgimage'], 'relative'); }?>><label for="checkbox2">Relative</label>
	                    </div>
	                    </fieldset>
	                     <hr>
	                  </div>
	                  <div class="row"> 
	                      <div class="large-12 columns">
	                      <input type="radio" name="<?php echo $genthemesfr_option_name;?>[bgradio]" value="bgcolor" id="bgcolorselect" <?php if(isset($options['bgradio'])) { checked( $options['bgradio'], 'bgcolor'); }?>>
	                      <label for="bgcolorselect">
	                      Background Color
	                      </label>
	                      </div>
	                      <div class="large-4 columns">
	                      	<input type="text" name="<?php echo $genthemesfr_option_name;?>[bgcolor]" id="link-color" value="<?php if(isset($options['bgcolor'])) { echo esc_attr($options['bgcolor']);  if(empty($options['bgcolor'])) { echo "";} }?>"/>
	  					  </div>
	  					  <div class="large-8 columns">
	  					  	<a href="#" class="pickcolor hide-if-no-js" id="link-color-example"></a>
						  	<input type="button" class="pickcolor button hide-if-no-js" value="<?php esc_attr_e( 'Select a Color', 'genthemesv1' ); ?>" />
						  	<div id="colorPickerDiv" style="z-index: 100; background:#eee; border:1px solid #ccc; position:absolute; display:none;"></div>
	                      </div> 
	                  </div>  
	                  <div class="row">
	                    <div class="large-12 columns">
	                      <input type="radio" name="<?php echo $genthemesfr_option_name;?>[bgradio]" value="bgpattern" id="bgpattern" <?php if(isset($options['bgradio'])) { checked( $options['bgradio'], 'bgpattern'); }?>><label for="bgpattern">Background Pattern</label>
	                    <select name="<?php echo $genthemesfr_option_name;?>[bgpattern]" id="<?php echo $genthemesfr_option_name;?>[bgpattern]">
	                    <?php  
						$dir = "../wp-content/themes/genthemesv1/theme-options/assets/img/pattern/";  
						if (is_dir($dir)) {
						    if ($dh = opendir($dir)) {
						        $images = array();
						
						        while (($file = readdir($dh)) !== false) {
						            if (!is_dir($dir.$file)) {
						                $images[] = $file;
						            }
						        }
						
						        closedir($dh);
						
						    foreach($images as $image) {
						  		echo '<option value="'.$image.'" '.selected( $options['bgpattern'], $image ).'>'.$image.'</option>';
								}
						    }
						}
	                    ?>
	                    </select>
	                    </div>
	                  </div>
	                  
                 	</div>
                 	
                 	<label for="checkbox1"><a href="#" class="label tiny radius 20 success pad-bottom">SIDEBAR PAGE::SINGLE</a></label>
	                 	<div class="row">
		                    <div class="large-12 columns">
		                    <?php 
		                    global $wp_registered_sidebars;  
							$sidebar_options = array(); $default_sidebar = '';
							foreach ($wp_registered_sidebars as $registered_sidebar) {
								$default_sidebar = empty($default_sidebar) ? $registered_sidebar['id'] : $default_sidebar;
								$sidebar_options[$registered_sidebar['id']] = $registered_sidebar['name'];
								?>
								<label for="<?php echo $registered_sidebar['id']; ?>">
								<input type="checkbox" id="<?php echo $registered_sidebar['id']; ?>" name="<?php echo $genthemesfr_option_name;?>[<?php echo $registered_sidebar['id'];?>]" value="<?php if(isset($registered_sidebar['id'])) { echo $registered_sidebar['id']; } ?>" 
								<?php if(isset($options["$registered_sidebar[id]"])) { checked( $options["$registered_sidebar[id]"], $registered_sidebar['id']); }?>>
								<?php echo $registered_sidebar['name'];?>
								</label> 
								<?php
							} 
		                    ?> 
		                    </div>
	                    </div>
	               </div> 
                    
                 </div>  
              <!-- BUTTON -->  
              <input type="submit" class="button-primary" value="Save Options"> <span id="progress"></span>
              <!-- //BUTTON -->
            </div>
          </form>
          </div> 
    </div>
  </div>
</div>
<?php
}
function genthemesfr_show_msg($message, $msgclass = 'info'){
	echo "<div id='message' class='$msgclass'>$message</div>";
}
function genthemesfr_validate_options( $input ) {
	
	if ( isset( $input['favicon'] ) ) {
		$valid['favicon'] = wp_filter_post_kses( $input['favicon'] );
	}
	if ( isset( $input['logo'] ) ) {
		$valid['logo'] = wp_filter_post_kses( $input['logo'] );
	}
	if ( isset( $input['description'] ) ) {
		$valid['description'] = wp_filter_post_kses( $input['description'] );
	}
	if ( isset( $input['keyword'] ) ) {
		$valid['keyword'] = wp_filter_post_kses( $input['keyword'] );
	}
	if ( isset( $input['bgradio'] ) ) {
		$valid['bgradio'] = wp_filter_post_kses( $input['bgradio'] );
	}
	if ( isset( $input['attr_bgimage'] ) ) {
		$valid['attr_bgimage'] = wp_filter_post_kses( $input['attr_bgimage'] );
	}
	if ( isset( $input['bgimage'] ) ) {
		$valid['bgimage'] = wp_filter_post_kses( $input['bgimage'] );
	}
	if ( isset( $input['bgpattern'] ) ) {
		$valid['bgpattern'] = wp_filter_post_kses( $input['bgpattern'] );
	}
	if ( isset( $input['bgcolor'] ) ) {
		$valid['bgcolor'] = wp_filter_post_kses( $input['bgcolor'] );
	}	
	
	return apply_filters( 'genthemesfr_validate_options', $input );   
}
function unique_identifyer_admin_notices() {
	if(isset($_GET['page'])){
	$enthemesfr_settings_pg = strpos($_GET['page'], GENTHEMES_PAGE_BASENAME);
	$set_errors = get_settings_errors(); 
	if(current_user_can ('manage_options') && $enthemesfr_settings_pg !== FALSE && !empty($set_errors)){ 
		if ($set_errors[0]['code'] == 'settings_updated' && isset( $_GET['settings-updated']) ){
			genthemesfr_show_msg("<p>" . $set_errors[0]['message'] . "</p>", 'updated');
		}else{
			// there maybe more than one so run a foreach loop.
			foreach($set_errors as $set_error){
				// set the title attribute to match the error "setting title" - need this in js file
				wptuts_show_msg("<p class='setting-error-message' title='" . $set_error['setting'] . "'>" . $set_error['message'] . "</p>", 'error');
			}
		}
	}}
}
add_action( 'admin_notices', 'unique_identifyer_admin_notices' );
endif; 