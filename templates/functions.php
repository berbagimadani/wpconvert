<?php
/**
 * Gen Themes functions and definitions.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
require_once ( get_stylesheet_directory() . '/theme-options/genthemes_net_options.php');
require_once ( get_stylesheet_directory() . '/functions/foundation4-topbar-menu.php'); 

if( file_exists( get_template_directory().'/class/foundation4-topbar-walker.php' ) ) {
	require_once ( get_stylesheet_directory() . '/class/foundation4-topbar-walker.php'); 
}
  
if ( ! isset( $content_width ) )
	$content_width = 604;
	$root = get_template_directory_uri(); 
 
function {name_fun}_scripts_styles() {
	global $root;
	{include_styles}
	{include_js} 
}
add_action( 'wp_enqueue_scripts', '{name_fun}_scripts_styles' );
/*{scripts_print}*/
function {name_fun}_setup() { 
	{include_file}
	/*{include_sidebar_remove}*/
	
	load_theme_textdomain( '{name_fun}', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', '{name_fun}' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', '{name_fun}_setup' );

function {name_fun}_widgets_init() {
	{register}
	register_sidebar( array(
		'name' => __( 'Main Sidebar', '{name_fun}' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', '{name_fun}' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	{register_sidebar}
	/*{create_sidebar_remove}*/
}
add_action( 'widgets_init', '{name_fun}_widgets_init' );
if ( ! function_exists( '{name_fun}_comment' ) ) :
function {name_fun}_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', '{name_fun}' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', '{name_fun}' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	 <!--{testing}-->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', '{name_fun}' ); ?></p>
			<?php endif; ?>  
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
function {name_fun}_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', '{name_fun}_enqueue_comments_reply' );
// Setting body background css
function opt_genthemes($var){
	$options = get_option('genthemesfr_options');
	if(isset($options[$var])){
		return $options[$var];
	}
}
function {name_fun}_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'body_genthemes';
	// return the $classes array
	return $classes;
}
add_filter('body_class','{name_fun}_class_names');
function {name_fun}_style_body(){
	$options = get_option('genthemesfr_options');
	$body_classs ="";
	if($options["bgradio"] == "bgimage"){
		if($options["attr_bgimage"] == "fixed"){
			$body_classs = "<style>
						body.body_genthemes{
							background: url('".$options['bgimage']."') no-repeat center center fixed; 
						  	-webkit-background-size: cover;
						  	-moz-background-size: cover;
						  	-o-background-size: cover;
						  	background-size: cover; 
						}
						</style>";
		}
	   if($options["attr_bgimage"] == "relative"){
			$body_classs = "<style>
						body.body_genthemes{
							background: url('".$options['bgimage']."') no-repeat scroll center top transparent; 
						  	background-repeat: repeat; /* default */
							background-repeat: repeat-x; /* repeat horizontally */
							background-repeat: repeat-y; /* repeat vertically */
							background-repeat: no-repeat; /* don't tile the image */ 
							background-color: ".$options['bgcolor'].";
						}
						</style>";
		}
	}
	if($options["bgradio"] == "bgpattern"){
		$body_classs = "<style>
						body.body_genthemes{
							background: url('".get_template_directory_uri()."/theme-options/assets/img/pattern/".$options['bgpattern']."');
							background-position: 50% 50%; /* image centered on screen */
							background-position: 50%; /* this also centers on screen */
							background-position: 100px 100px;
							background-position: center;
						}
						</style>";
	}
	if($options["bgradio"] == "bgcolor"){
		$body_classs = "<style>
						body.body_genthemes{
							background: ".$options['bgcolor'].";
						}
						</style>";
	}
    
	
	return print $body_classs;
}
if ( ! function_exists( '{name_fun}_content_nav' ) ) :
function {name_fun}_content_nav( $html_id ) {
	global $wp_query;   
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<ul class="pagination">
		<?php 
				$big = 999999999; // need an unlikely integer 
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) ); 
		?>
	</ul>
	<?php endif;
} 
endif;

/* VIEW POST */
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function view_count($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');
/* END VIEW POST */