<?php
/**
 * Gen Themes Display.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area"> 
	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		{title_comment}
		  
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="navigation" role="navigation">
			<h1 class="assistive-text section-heading"><?php _e( 'Comment navigation', 'genthemesv1' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'genthemesv1' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'genthemesv1' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a note.
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Comments are closed.' , 'genthemesv1' ); ?></p>
		<?php endif; ?>

		<?php endif; // have_comments() ?> 
		
		<?php 
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$comments_args = array( 
	        'label_submit'=>'{label_button}',  
			'title_reply'       => __( '{form_title1}','genthemesv1'),
	  		'title_reply_to'    => __( 'Leave a Reply to %s','genthemesv1'),
	  		'cancel_reply_link' => __( 'Cancel','genthemesv1'),
			'comment_notes_before' => '{form_title2}',
	        'comment_notes_after' => '',
		 	'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
	        // redefine your own textarea (the comment body)
	        'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . __( 'Comment', 'noun' ) .
	    	'</label><textarea id="comment" name="comment" class="custome_textarea" cols="45" rows="8" aria-required="true">' .
	    	'</textarea></p>',
			'id_submit' => '{id_button}', 
	        'fields' => apply_filters( 'comment_form_default_fields', array( 
				    'author' =>'{form_title_author}',
				
				    'email' =>'{form_title_email}',
		
					'url' =>'{form_title_url}',
				    )
				  ),
		);
	
		comment_form($comments_args);
		?>

</div><!-- #comments .comments-area -->