<?php
/**
 * Gen Themes functions and definitions.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
require_once ( get_stylesheet_directory() . '/theme-options/genthemes_net_options.php');
require_once ( get_stylesheet_directory() . '/functions/foundation4-topbar-menu.php'); 

if( file_exists( get_template_directory().'/class/foundation4-topbar-walker.php' ) ) {
	require_once ( get_stylesheet_directory() . '/class/foundation4-topbar-walker.php'); 
}
  
if ( ! isset( $content_width ) )
	$content_width = 604;
	$root = get_template_directory_uri(); 
 
function genthemesblog_scripts_styles() {
	global $root;
		wp_enqueue_style( 'foundation', $root . '/css/foundation.css', array(), false);	wp_enqueue_style( 'styles', $root . '/css/styles.css', array(), false);	wp_enqueue_style( 'menu', $root . '/css/menu.css', array(), false);	wp_enqueue_style( 'hover', $root . '/css/hover.css', array(), false);	wp_enqueue_style( 'responsive', $root . '/css/responsive.css', array(), false);	wp_enqueue_style( 'owl.carousel', $root . '/css/owl.carousel.css', array(), false);	wp_enqueue_style( 'font-awesome', $root . '/css/font-awesome.css', array(), false);	wp_enqueue_style( 'Open+Sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700');	wp_enqueue_style( 'Duru+Sans', 'http://fonts.googleapis.com/css?family=Duru+Sans:400italic,600italic,400,600,700');	wp_enqueue_style( 'Julius+Sans+One', 'http://fonts.googleapis.com/css?family=Julius+Sans+One');
		wp_enqueue_script('jquery');	wp_enqueue_script( 'owl.carousel', $root . '/js/owl.carousel.js', array('jquery'), false, true );	wp_enqueue_script( 'jquery.marquee', $root . '/js/jquery.marquee.js', array('jquery'), false, true );	wp_enqueue_script( 'foundation.min', $root . '/js/foundation.min.js', array('jquery'), false, true );	wp_enqueue_script( 'foundation', $root . '/js/foundation', array('jquery'), false, true );	wp_enqueue_script( 'modernizr', $root . '/js/modernizr.js', array('jquery'), false, true ); 
}
add_action( 'wp_enqueue_scripts', 'genthemesblog_scripts_styles' );
function genthemesblog_scripts_print() {
?>
    <script>
    $(function(){
      var $mwo = $('.marquee-with-options');
      $('.marquee').marquee();
      $('.marquee-with-options').marquee({
        //speed in milliseconds of the marquee
        speed: 19000,
        //gap in pixels between the tickers
        gap: 0,
        //gap in pixels between the tickers
        delayBeforeStart: 0,
        //'left' or 'right'
        direction: 'left',
        //true or false - should the marquee be duplicated to show an effect of continues flow
        duplicated: true,
        //on hover pause the marquee - using jQuery plugin https://github.com/tobia/Pause
        pauseOnHover: true
      });
      
       
    });
    </script>
    <script>
      /*$(document).foundation(); */
      $(document).foundation({
        orbit: {
          animation: 'slide',
          timer_speed: 4000,
          pause_on_hover: true,
          animation_speed: 500,
          navigation_arrows: true,
          bullets: false,
          next_on_click: true, 
        }
      });
    </script>
    
    <script>
    $(document).ready(function() {
      var owl = $("#gen-carousel");
      $("#gen-carousel").owlCarousel({
        autoPlay: false,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
      });
      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })

    });
    </script> 
    <?php } 
add_action( 'wp_footer', 'genthemesblog_scripts_print', 100);

function genthemesblog_setup() { 
	
	require( get_template_directory() . '/widgets/Home_Page_AdsTop_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Headline_News.php' );
	require( get_template_directory() . '/widgets/Home_Slider.php' );
	require( get_template_directory() . '/widgets/Home_Lates_One.php' );
	require( get_template_directory() . '/widgets/Home_Lates_Two.php' );
	require( get_template_directory() . '/widgets/Home_Lates_Three.php' );
	require( get_template_directory() . '/widgets/Home_Lates_Four.php' );
	require( get_template_directory() . '/widgets/Home_Carousel_Five.php' );
	require( get_template_directory() . '/widgets/Home_Lates_Six.php' );
	
	require( get_template_directory() . '/widgets/Popular_Widget.php' );
	require( get_template_directory() . '/widgets/Ads_Widget.php' );
	require( get_template_directory() . '/widgets/Facebook_Widget.php' );
	require( get_template_directory() . '/widgets/Category_Post_Widget.php' );
	
	load_theme_textdomain( 'genthemesblog', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', 'genthemesblog' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'genthemesblog_setup' );

function genthemesblog_widgets_init() {
	
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'genthemesblog' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'genthemesblog' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Home Page AdsTop Widget', 'genthemesv1' ),
		'id' => 'home_page_adstop_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Headline News', 'genthemesv1' ),
		'id' => 'home_headline_news',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Slider', 'genthemesv1' ),
		'id' => 'home_slider',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Lates One', 'genthemesv1' ),
		'id' => 'home_lates_one',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Lates Two', 'genthemesv1' ),
		'id' => 'home_lates_two',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Lates Three', 'genthemesv1' ),
		'id' => 'home_lates_three',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Lates Four', 'genthemesv1' ),
		'id' => 'home_lates_four',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Carousel Five', 'genthemesv1' ),
		'id' => 'home_carousel_five',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Lates Six', 'genthemesv1' ),
		'id' => 'home_lates_six',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar First', 'genthemesblog' ),
		'id' => 'sidebar-first',
		'description' => __( 'tes-lagi-ah', 'genthemesblog' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
}
add_action( 'widgets_init', 'genthemesblog_widgets_init' );
if ( ! function_exists( 'genthemesblog_comment' ) ) :
function genthemesblog_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'genthemesblog' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'genthemesblog' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	 
                    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                    <article id="comment-2" class="comment">
                      <header class="comment-meta comment-author vcard">  
                          <div class="box-list-image-comment">
                             <?php echo get_avatar( $comment, 40 );?>
                          </div>

                          <div class="box-list-comment"> 
                            <div class="box-detail-comment">
                              <div class="box-date-comment">  
                                <div class="box-edit-comment-left">
                                <cite><b class="fn"><?php echo get_comment_author_link();?></b></cite> 
                                <time datetime="2013-12-18T08:04:06+00:00">» <?php echo get_comment_date();?></time>
                                </div>
                              </div> 
                              <div class="box-edit-comment">
                                <div class="box-edit-comment-left edit">»
                                  <?php	edit_comment_link('Edit');?> 
                                </div>
                                <div class="box-edit-comment-right reply"> | 
                                 <?php comment_reply_link( array_merge( $args, array( 'reply_text' => 'Reply', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );?>
                                 <span>↓</span>
                                </div>
                              </div>
                            </div>
                            <div class="comment-content">
                              <p><?php comment_text(); ?></p> 
                            </div><!-- .comment-content --> 
                           </div> 
                      </header><!-- .comment-meta --> 

                    </article><!-- #comment-## -->  
                    </li><!-- #comment-## --> 
                  

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'genthemesblog' ); ?></p>
			<?php endif; ?>  
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
function genthemesblog_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', 'genthemesblog_enqueue_comments_reply' );
// Setting body background css
function opt_genthemes($var){
	$options = get_option('genthemesfr_options');
	if(isset($options[$var])){
		return $options[$var];
	}
}
function genthemesblog_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'body_genthemes';
	// return the $classes array
	return $classes;
}
add_filter('body_class','genthemesblog_class_names');
function genthemesblog_style_body(){
	$options = get_option('genthemesfr_options');
	$body_classs ="";
	if($options["bgradio"] == "bgimage"){
		if($options["attr_bgimage"] == "fixed"){
			$body_classs = "<style>
						body.body_genthemes{
							background: url('".$options['bgimage']."') no-repeat center center fixed; 
						  	-webkit-background-size: cover;
						  	-moz-background-size: cover;
						  	-o-background-size: cover;
						  	background-size: cover; 
						}
						</style>";
		}
	   if($options["attr_bgimage"] == "relative"){
			$body_classs = "<style>
						body.body_genthemes{
							background: url('".$options['bgimage']."') no-repeat scroll center top transparent; 
						  	background-repeat: repeat; /* default */
							background-repeat: repeat-x; /* repeat horizontally */
							background-repeat: repeat-y; /* repeat vertically */
							background-repeat: no-repeat; /* don't tile the image */ 
							background-color: ".$options['bgcolor'].";
						}
						</style>";
		}
	}
	if($options["bgradio"] == "bgpattern"){
		$body_classs = "<style>
						body.body_genthemes{
							background: url('".get_template_directory_uri()."/theme-options/assets/img/pattern/".$options['bgpattern']."');
							background-position: 50% 50%; /* image centered on screen */
							background-position: 50%; /* this also centers on screen */
							background-position: 100px 100px;
							background-position: center;
						}
						</style>";
	}
	if($options["bgradio"] == "bgcolor"){
		$body_classs = "<style>
						body.body_genthemes{
							background: ".$options['bgcolor'].";
						}
						</style>";
	}
    
	
	return print $body_classs;
}
if ( ! function_exists( 'genthemesblog_content_nav' ) ) :
function genthemesblog_content_nav( $html_id ) {
	global $wp_query;   
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<ul class="pagination">
		<?php 
				$big = 999999999; // need an unlikely integer 
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) ); 
		?>
	</ul>
	<?php endif;
} 
endif;

/* VIEW POST */
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function view_count($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');
/* END VIEW POST */