jQuery(document).ready(function() {
    jQuery('#colorpicker-bgcolor').hide();
    jQuery('#colorpicker-bgcolor').farbtastic('#bgcolor');

    jQuery('#bgcolor').click(function() {
        jQuery('#colorpicker-bgcolor').fadeIn();
    });

    jQuery(document).mousedown(function() {
        jQuery('#colorpicker-bgcolor').each(function() {
            var display = jQuery(this).css('display');
            if ( display == 'block' )
                jQuery(this).fadeOut();
        });
    });

});