<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Ads_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Ads_Widget'; 
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function Ads_Widget() {
		$widget_ops = array( 'classname' => 'Ads_Widget', 'description' => __( 'A Ads Widget of your site&rsquo;s Posts.', 'genthemesv1' ) );
		$this->WP_Widget('Ads_Widget', __('Ads Widget', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP);
		 
		
		$image_ads = apply_filters( 'widget_image_ads', empty($instance['image_ads'] ) ? __('', 'genthemesv1') : $instance['image_ads'], $instance, $this->id_base);
		
		$url_image_ads = apply_filters( 'widget_url_image_ads', empty($instance['url_image_ads'] ) ? __('', 'genthemesv1') : $instance['url_image_ads'], $instance, $this->id_base);
		 
 			  ?> 
				 
      		 
                      <!-- ADS -->
                      <div class="block-side-2">   
                      <ul class="large-block-grid-1 medium-block-grid-1 small-block-grid-1">
                        <li>
                           <a href="<?php echo $url_image_ads; ?>">
                            <img src="<?php echo $image_ads; ?>"> 
                           </a>
                        </li> 
                      </ul> 
                     </div>
                    <!-- // ADS -->
                    
      
 			<?php 
			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata(); 
			// end check for ephemeral posts 
			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;  
		
		$instance['image_ads'] = strip_tags( $new_instance['image_ads']);
		
		$instance['url_image_ads'] = esc_html( $new_instance['url_image_ads']);
		
	
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {  
		
		$url_image_ads = isset( $instance['url_image_ads']) ? esc_html( $instance['url_image_ads'] ) : '';
		$image_ads = isset( $instance['image_ads']) ? esc_attr( $instance['image_ads'] ) : '';
	?>
		 
				<p><label for="<?php echo esc_attr($this->get_field_id( 'url_image_ads' ) ); ?>"><?php _e( 'url_image_ads:', 'genthemesv1'); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'url_image_ads' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'url_image_ads' ) ); ?>" type="text" value="<?php echo esc_attr($url_image_ads); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'image_ads' ) ); ?>"><?php _e( 'image_ads:', 'genthemesv1'); ?></label></p>
		<input type="text" class="widefat image_ads" id="<?php echo esc_attr($this->get_field_id( 'image_ads' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'image_ads' ) ); ?>" value="<?php echo esc_attr($image_ads); ?>"/>	
			<div class="image_ads">
			<?php 
			if(!empty($image_ads)){
			echo '<img src="'.$image_ads.'" style="width:100%">';
			}
			?>
			</div>
			<input type="button" id="select-image_ads" class="upload_image_button button" value="Select Image" name="image_ads">

		 
	<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Ads_Widget");') );

function Ads_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Ads_Widget_scripts');