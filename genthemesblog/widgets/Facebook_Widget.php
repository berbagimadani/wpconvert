<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Facebook_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Facebook_Widget'; 
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function Facebook_Widget() {
		$widget_ops = array( 'classname' => 'Facebook_Widget', 'description' => __( 'A Facebook Widget of your site&rsquo;s Posts.', 'genthemesv1' ) );
		$this->WP_Widget('Facebook_Widget', __('Facebook Widget', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP);
		 
		
		
		$title_facebook = apply_filters( 'widget_title_facebook', empty($instance['title_facebook'] ) ? __('', 'genthemesv1') : $instance['title_facebook'], $instance, $this->id_base);
		$input_facebook_html = apply_filters( 'widget_input_facebook_html', empty($instance['input_facebook_html'] ) ? __('', 'genthemesv1') : $instance['input_facebook_html'], $instance, $this->id_base);
		 
 			  ?> 
				 
      		 
                      <!-- TITLE CONTENT -->
                      <div class="sidebarnav">
                      	<h3><span class="title-bold"><?php echo html_entity_decode($title_facebook); ?></span></h3>
                      </div>
                      <!-- //TITLE CONTENT -->
                      <!-- CONTENT -->
                      <div class="row row-pad">
                        <div class="large-12 medium-12 columns">
                          <?php echo html_entity_decode($input_facebook_html); ?> 
                        </div>
                      </div> 
                      <!-- // CONTENT --> 
                    
      
 			<?php 
			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata(); 
			// end check for ephemeral posts 
			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;  
		
		
		$instance['title_facebook'] = esc_html( $new_instance['title_facebook']);
		$instance['input_facebook_html'] = esc_html( $new_instance['input_facebook_html']);
		
	
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {  
		
		$title_facebook = isset( $instance['title_facebook']) ? esc_html( $instance['title_facebook'] ) : '';
		$input_facebook_html = isset( $instance['input_facebook_html']) ? esc_html( $instance['input_facebook_html'] ) : '';
	?>
		 
				<p><label for="<?php echo esc_attr($this->get_field_id( 'title_facebook' ) ); ?>"><?php _e( 'title_facebook:', 'genthemesv1'); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title_facebook' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'title_facebook' ) ); ?>" type="text" value="<?php echo esc_attr($title_facebook); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'input_facebook_html' ) ); ?>"><?php _e( 'input_facebook_html:', 'genthemesv1'); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'input_facebook_html' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'input_facebook_html' ) ); ?>" type="text" value="<?php echo esc_attr($input_facebook_html); ?>"/></p>

		 
	<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Facebook_Widget");') );

function Facebook_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Facebook_Widget_scripts');