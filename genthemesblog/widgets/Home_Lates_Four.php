<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Home_Lates_Four extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Home_Lates_Four';   
	public $col = '';
	 
	function Home_Lates_Four() {
		$widget_ops = array( 'classname' => 'Home_Lates_Four', 'description' => __( 'A Home Lates Four of your site&rsquo;s Posts.', 'genthemesv1' ) );
		$this->WP_Widget( $this->slugwidget, __('Home Lates Four', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP );
	
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( '', 'genthemesv1' ) : $instance['title'], $instance, $this->id_base);
		$type = apply_filters( 'widget_type', empty( $instance['type'] ) ? __( '', 'genthemesv1' ) : $instance['type'], $instance, $this->id_base);
		
		if ( ! isset( $instance['number'] ) )
			$instance['number'] = '10';
		if ( ! $number = absint( $instance['number'] ) )
 			$number = 10;
 			
 		if ( ! isset( $instance['coloumn'] ) )
			$instance['coloumn'] = $this->col;

		if ( ! $coloumn = absint( $instance['coloumn'] ) )
 			$coloumn = $this->col;	
  
 			  ?> 
				  
                  <div class="large-6 medium-6 columns">
                    <div class="sidebarnav"><h3><span class="title-bold"><?php echo $title; ?></span></h3></div>
                    <ul class="small-block-grid-1">
                        <?php
					$query = array(
						'post_type' => $this->post_type,
						 $this->taxonomy =>  $type, 
						'order' => 'DESC',
						'posts_per_page' => $number
						);
						$pageposts = new WP_Query($query); 
						while ( $pageposts->have_posts() ) :
								$pageposts->the_post();
								$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
								$featured_image = $featured_image_array[0];
						?>
                        <li>
                          <a href="<?php echo get_permalink();?>"> 
                          
							<?php 
							$default_attr = array( 
							'class'	=> "",
							'alt'	=> trim(strip_tags(get_the_excerpt())),
							'title'	=> trim(strip_tags( get_the_title())),
							);
							?>
							<?php 
							if ( has_post_thumbnail()) {
							   echo get_the_post_thumbnail(get_the_ID(), 'large', $default_attr); 
							}
							 else{
							 	echo '<img src="'.opt_genthemes('thumbnail').'">';
							 }
				?>
                          </a>
                          <div class="panel-line"> 
                            <h4><a href="<?php echo get_permalink();?>"><small> <?php echo get_the_title();?></small></a></h4> 
                            <span class="datetime"><i class="fa fa-calendar"></i>  <?php echo get_the_modified_date()?></span> 
                            <span class="num-comment"><i class="fa fa-comments"></i> <?php comments_popup_link( '0 Comments', '1 Comments', '% Comments', '', '0 Comments'); ?></span> 
                            <span class="num-view"><i class="fa fa-eye"></i> <?php echo view_count(get_the_ID())?> Views</span> 
                          </div>
                          <div class="panel-description">
                            <p>
                               <?php echo wp_trim_words( get_the_content(),15 );?>
                            </p>
                          </div>
                        </li> 
                        <?php endwhile; ?>
                    </ul>
                  </div>
                  
 			<?php 
			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata(); 
			// end check for ephemeral posts 
			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['type'] = strip_tags( $new_instance['type'] );
		$instance['coloumn'] = (int) $new_instance['coloumn'];
		$instance['number'] = (int) $new_instance['number'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {
		$title = isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$type = isset( $instance['type']) ? esc_attr( $instance['type'] ) : ''; 
		$coloumn = isset( $instance['coloumn']) ? absint( $instance['coloumn'] ) : 7;
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 10;
?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'genthemesv1' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>"><?php _e( 'Category:', 'genthemesv1' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>" >
			<?php 
				$terms = get_terms('category');
				$count = count($terms);
				if ( $count > 0 ){ 
				     foreach ( $terms as $term ) {
				       //echo "<li>" . $term->slug . "</li>";
				       ?>
				       <option value="<?php echo $term->slug ?>" <?php if($type == $term->slug) echo "selected";?>>
				       <?php echo $term->slug; ?>
				       </option> 
				        <?php
				        
				     } 
				}
			?>
			</select>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'genthemesv1' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Home_Lates_Four");') );

function Home_Lates_Four_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Home_Lates_Four_scripts');