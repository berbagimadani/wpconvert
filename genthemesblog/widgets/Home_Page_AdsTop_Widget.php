<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Home_Page_AdsTop_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Home_Page_AdsTop_Widget'; 
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function Home_Page_AdsTop_Widget() {
		$widget_ops = array( 'classname' => 'Home_Page_AdsTop_Widget', 'description' => __( 'A Home Page AdsTop Widget of your site&rsquo;s Posts.', 'genthemesv1' ) );
		$this->WP_Widget('Home_Page_AdsTop_Widget', __('Home Page AdsTop Widget', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP);
		 
				$home_image_ads_top = apply_filters( 'widget_home_image_ads_top', empty($instance['home_image_ads_top'] ) ? __('', 'genthemesv1') : $instance['home_image_ads_top'], $instance, $this->id_base);
		
		 
 			  ?> 
				 
      		 
                    <img src="<?php echo $home_image_ads_top; ?>">
                   
      
 			<?php 
			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata(); 
			// end check for ephemeral posts 
			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;  
				$instance['home_image_ads_top'] = strip_tags( $new_instance['home_image_ads_top']);
		
		
	
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {  
				$home_image_ads_top = isset( $instance['home_image_ads_top']) ? esc_attr( $instance['home_image_ads_top'] ) : '';
	?>
		 
				<p><label for="<?php echo esc_attr($this->get_field_id( 'home_image_ads_top' ) ); ?>"><?php _e( 'home_image_ads_top:', 'genthemesv1'); ?></label></p>
		<input type="text" class="widefat home_image_ads_top" id="<?php echo esc_attr($this->get_field_id( 'home_image_ads_top' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'home_image_ads_top' ) ); ?>" value="<?php echo esc_attr($home_image_ads_top); ?>"/>	
			<div class="home_image_ads_top">
			<?php 
			if(!empty($home_image_ads_top)){
			echo '<img src="'.$home_image_ads_top.'" style="width:100%">';
			}
			?>
			</div>
			<input type="button" id="select-home_image_ads_top" class="upload_image_button button" value="Select Image" name="home_image_ads_top">

		 
	<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Home_Page_AdsTop_Widget");') );

function Home_Page_AdsTop_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Home_Page_AdsTop_Widget_scripts');