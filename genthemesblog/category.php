
      <?php get_header(); ?>
      <?php if (is_category( )) {?> 
        <!-- ROW GEN 2 -->
        <div class="row-gen-2">
          <div class="row">
            <div class="large-12 column mar-top-right-30">
              <ul class="breadcrumbs">
                <?php 
				if (!is_home()) { 
					if (is_category() || is_single()) {
						echo  the_category(' '); 
					}	 
				} ?>
              </ul>
            </div>
            <div class="large-8 medium-8 small-12 column">
              <!-- CONTENT -->
              <div> 
                <div class="sidebarnav">
                  <h3>
                    <div class="title-bold">
				<?php 
					$cat = get_query_var('cat');
					$catName = get_category ($cat);
					echo $catName->cat_name;
				?>
                    </div>
                  </h3>
                </div>
                <div class="row row-pad">
                    <!-- ROW -->
                    <?php  
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
					    'post_type' => 'post',
						'category_name' =>  $catName->slug,
					    'paged'=> $paged,
						'order' => 'DESC', 
					);  
					$the_query = new WP_Query( $args ); 
						while ($the_query->have_posts()):
						$the_query->the_post();   
						$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
						$featured_image = $featured_image_array[0];
					?>
                    <!-- ROW CO -->
                    <div class="large-12 medium-12 columns">
                        <div class="sidebarnav-img-left">
                          <a href="<?php echo get_permalink();?>"> 
                            <?php 
								$default_attr = array( 
								'class'	=> "img-pad",
								'alt'	=> trim(strip_tags(get_the_excerpt())),
								'title'	=> trim(strip_tags( get_the_title())),
								);
								?>
								<?php 
								if ( has_post_thumbnail()) {
								   echo get_the_post_thumbnail(get_the_ID(), 'medium', $default_attr); 
								}
								 else{
								 	echo '<img src="'.opt_genthemes('thumbnail').'">';
								 }
							?>
                          </a>
                        </div>
                        <div class="sidebarnav-text-left">
                          <div class="panel-line"> 
                            <h4><a href="<?php echo get_permalink();?>"><small><?php echo get_the_title();?></small></a></h4> 
                            <span class="datetime"><i class="fa fa-user"></i> <?php echo get_the_author_meta('nickname');?> </span> 
                            <span class="datetime"><i class="fa fa-calendar"></i> <?php echo get_the_modified_date()?></span> 
                            <span class="num-comment"><i class="fa fa-comments"></i>  <?php comments_popup_link( '0 Comments', '1 Comments', '% Comments', '', '0 Comments'); ?></span> 
                            <span class="num-view"><i class="fa fa-eye"></i> <?php echo view_count(get_the_ID())?></span> 
                          </div>
                        </div>
                        <div class="panel-description">
                          <p>
                            <?php echo wp_trim_words( get_the_content(),11 );?>
                          </p>
                        </div>
                    </div>
                    <!-- //ROW CO -->
                    <?php endwhile;?>

                    <!-- END ROW --> 
                </div>
              </div>
              <!-- //CONTENT -->

              <div class="panel-ads"> 
                <img src="img/adsense728x90.gif" />
              </div>
              <ul class="pagination">
                
				<?php 
					$big = 999999999; // need an unlikely integer 
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $the_query->max_num_pages
					) ); 
				?>
	
              </ul> 
            </div>
          <?php } ?>
          <?php get_sidebar(); ?>
          <?php get_footer(); ?>
          