
        <?php if ( is_single() ) : ?>
 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
        <!-- ROW GEN 2 -->
        <div class="row-gen-2">
          <div class="row">
            <div class="large-12 column mar-top-right-30">
              <ul class="breadcrumbs">
                
		<?php 
		if (!is_home()) { 
			if (is_category() || is_single()) {
			echo  the_category(' ');
			echo '<li>'.get_the_title().'</li>';
			}	 
		}
		?>
              </ul>
            </div>
            <div class="large-8 medium-8 small-12 column">
              <!-- CONTENT -->
              <div> 
                <div class="sidebarnav"><h3><div class="title-bold"><?php $category = get_the_category(); echo $category[0]->cat_name;?></div></h3></div>
                <ul class="large-block-grid-1 medium-block-grid-1 small-block-grid-1">
                  <li>
                     
				<?php 
				if ( has_post_thumbnail()) {
					echo get_the_post_thumbnail(get_the_ID(), 'full');
				} 
				?>
				 
                     <div class="panel-line"> 
                        <h2><small><?php the_title();?></small></h2> 
                        <span class="datetime"><i class="fa fa-user"></i> admin </span> 
                        <span class="datetime"><i class="fa fa-calendar"></i> Sep 12 2013</span> 
                        <span class="num-comment"><i class="fa fa-comments"></i> <a href="#">20 Comments</a></span> 
                        <span class="num-view"><i class="fa fa-eye"></i> 20 Views</span> 
                      </div>
                  </li> 
                </ul>
                <?php echo the_content();?> 
              </div>

              
              <!-- //CONTENT --> 
              <div class="panel-ads"> 
                <img src="img/adsense728x90.gif" />
              </div> 
               
              <div class="row">
                <div class="large-8 medium-8 small-8 column">
                  <h5> 
                    <small class="panel tab"><?php the_tags('',''); ?></small> 
                  </h5> 
                </div>
                <div class="large-3 medium-3small-3 column">
                  <div class="single-share">
                     FB, GO+, TWI TES
                  </div>
                </div>
              </div>
              
              <!-- RELATED -->
              <div class="row">
                <div class="large-12 columns">
                  <div class="sidebarnav"><h3><span class="title-bold">RELATED</span> POSTS</h3></div>
                  <ul class="large-block-grid-4 small-block-grid-2 small-block-grid-1">
                    <?php
		 			$category = get_the_category();  
					$query = array(
						'post_type' => 'post',
						'category_name'=>  $category[0]->slug, 
						'order' => 'DESC', 
						'post__not_in' => array($post->ID),
						'posts_per_page' => '4'
						);
						$pageposts = new WP_Query($query); 
						while ( $pageposts->have_posts() ) :
								$pageposts->the_post();
								$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
								$featured_image = $featured_image_array[0];
					?>
                    <li>
                      <a href="<?php echo get_permalink();?>"> 
                        <?php 
								$default_attr = array( 
								'class'	=> "",
								'alt'	=> trim(strip_tags(get_the_excerpt())),
								'title'	=> trim(strip_tags( get_the_title())),
								);
								?>
								<?php 
								if ( has_post_thumbnail()) {
								   echo get_the_post_thumbnail(get_the_ID(), 'medium', $default_attr); 
								} 
								?>
                      </a>
                      <div class="panel-line"> 
                        <h4><a href="#"><small><?php echo get_the_title();?></small></a></h4> 
                        <span class="datetime"><i class="fa fa-calendar"></i> <?php echo get_the_modified_date()?></span> 
                        <span class="num-comment"><i class="fa fa-comments"></i> <?php comments_popup_link( '0 Comments', '1 Comments', '% Comments', '', '0 Comments'); ?></span> 
                        <span class="num-view"><i class="fa fa-eye"></i> <?php echo view_count(get_the_ID())?> Views</span> 
                      </div>
                      <div class="panel-description">
                        <p>
                          <?php echo wp_trim_words( get_the_content(),11 );?>
                        </p>
                      </div>
                    </li>
                    <?php endwhile;?> 
                    </ul>
                </div>
              </div>
              <!-- // RELATED -->
             

              <div class="row"> 
                <!-- COMMENT -->
                <div class="large-12 medium-12 small-12 column">
                 <div class="sidebarnav"><h3><div class="title-bold">LEAVE COMMENT</div></h3></div>
                   <?php comments_template( '', true); ?> 
                  </div> 
                </div>
                <!-- //COMMENT -->
              </div>
          <article>
    <?php endif;?> 
          