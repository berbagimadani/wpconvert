<?php 
/**
* Gen Themes Display.
* @package WordPress 
* @subpackage Genthemes V1
* @since genthemes v1
* @web genthemes.net
* @email genthemes@gmail.com
*/
?>
<!doctype html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo opt_genthemes('favicon');?>" >
    <link rel="apple-touch-icon" href="<?php echo opt_genthemes('favicon');?>" >
    <title><?php bloginfo().wp_title( '|', true, 'left' ); ?></title>
    

    <?php wp_head(); ?> 
  <?php $root = get_template_directory_uri(); ?> 
  <?php genthemesblog_style_body();?>
  <?php echo opt_genthemes('google-analytic');?>
    
  </head>

<body <?php body_class('home blog'); ?>> 

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=430707847037600";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

<div class="row-gen-wide-"> <!-- WIDE -->
  <div class="row">  <!-- ROW -->
      <div class="row-gen-boxed">  <!-- Boxed -->
        <!-- HEADER -->
        <div class="row">
          <div class="mar-top-10">
            <div class="large-2 medium-2 small-12 columns">
              <div class="left">
               <a href="<?php echo get_permalink('logo') ?>" ><img src="<?php echo opt_genthemes('logo');?>"></a>   
              </div>
            </div>
            <div class="large-10 medium-10 small-12 columns">
              <div class="right"> 
                	<?php if ( is_active_sidebar( 'home_page_adstop_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_adstop_widget' ); ?>
			<?php endif; ?><!-- ============== ////WIDGET CUSTOME-->
              </div>
            </div>
          </div>
        </div>
        <!-- //HEADER -->

      <!-- ROW GEN 1 -->
      <div class="row">
        <div class="mar-top-10">
          <div class="large-12 medium-12 small-12 column">
            <div class="row-gen-1">
              <div class="large-12 medium-12 small-12 column">
                <!-- MENU -->
                <div class="block-menu"> 
                  <nav class="top-bar" data-topbar> 
                    <ul class="title-area">
                    <li class="name">
                       
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
                    </ul>
                    <section class="top-bar-section">  
                      <?php foundation_head_bar();?> <!-- ====== //CREATE MENU ====== -->
                    </section>
                  </nav>
                </div>
                <!-- //MENU -->
              </div>
            </div>
          </div>
        </div>
        
        	<?php if ( is_active_sidebar( 'home_headline_news' ) ) : ?>
				<?php dynamic_sidebar( 'home_headline_news' ); ?>
			<?php endif; ?><!-- ====== //CREATE WGT  ==-->

      </div> 
      <!-- //ROW GEN 1 -->
