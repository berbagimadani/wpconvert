<?php
/**
 * Gen Themes Display.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area"> 
	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		
         <h3>
           <?php
    		printf( _n( 'One thought on &ldquo;%2&rdquo;', '%1 comments on &ldquo;%2&rdquo;', get_comments_number(), 'genthemesv1' ),
			number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
         </h3>
          		<ol class="commentlist">
		<?php wp_list_comments( array( 'callback' => 'genthemesrocia_comment', 'style' => 'ol' ) ); ?>
		</ol>
		
		  
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="navigation" role="navigation">
			<h1 class="assistive-text section-heading"><?php _e( 'Comment navigation', 'genthemesv1' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'genthemesv1' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'genthemesv1' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a note.
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Comments are closed.' , 'genthemesv1' ); ?></p>
		<?php endif; ?>

		<?php endif; // have_comments() ?> 
		
		<?php 
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$comments_args = array( 
	        'label_submit'=>'comment donk',  
			'title_reply'       => __( '<h3>Write a Reply or Comment tes donk</h3>','genthemesv1'),
	  		'title_reply_to'    => __( 'Leave a Reply to %s','genthemesv1'),
	  		'cancel_reply_link' => __( 'Cancel','genthemesv1'),
			'comment_notes_before' => '<p class="comment-notes">Your email address will not be published. Required fields are marked<span class="required">*</span></p>',
	        'comment_notes_after' => '',
		 	'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
	        // redefine your own textarea (the comment body)
	        'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . __( 'Comment', 'noun' ) .
	    	'</label><textarea id="comment" name="comment" class="custome_textarea" cols="45" rows="8" aria-required="true">' .
	    	'</textarea></p>',
			'id_submit' => 'button', 
	        'fields' => apply_filters( 'comment_form_default_fields', array( 
				    'author' =>'
            <p class="comment-form-author">
              <label for="author">Name '.( $req ? '<span class="required">*</span>' : '' ).'</label>
              <input type="text" id="author" name="author" value="'.esc_attr(  $commenter['comment_author'] ).'" size="30" '.$aria_req.'> 
            </p>',
				
				    'email' =>'
            <p class="comment-form-email">
              <label for="email">Email '.( $req ? '<span class="required">*</span>' : '' ).'</label>
              <input type="text" id="email" name="email" value="'.esc_attr(  $commenter['comment_author_email'] ).'" size="30" '.$aria_req.' >
            </p>',
		
					'url' =>'',
				    )
				  ),
		);
	
		comment_form($comments_args);
		?>

</div><!-- #comments .comments-area -->