<?php
/**
 * Gen Themes functions and definitions.
 * @package WordPress
 * @subpackage Genthemes V1
 * @since genthemes v1
 * @web genthemes.net
 * @email genthemes@gmail.com
 */
require_once ( get_stylesheet_directory() . '/theme-options/genthemes_net_options.php');
require_once ( get_stylesheet_directory() . '/functions/foundation4-topbar-menu.php'); 

if( file_exists( get_template_directory().'/class/foundation4-topbar-walker.php' ) ) {
	require_once ( get_stylesheet_directory() . '/class/foundation4-topbar-walker.php'); 
}
  
if ( ! isset( $content_width ) )
	$content_width = 604;
	$root = get_template_directory_uri(); 
 
function genthemesrocia_scripts_styles() {
	global $root;
		wp_enqueue_style( 'foundation', $root . '/assets/css/foundation.css', array(), '001' );	wp_enqueue_style( 'styles', $root . '/assets/css/styles.css', array(), '001' );	wp_enqueue_style( 'menu', $root . '/assets/css/menu.css', array(), '001' );	wp_enqueue_style( 'hover', $root . '/assets/css/hover.css', array(), '001' );	wp_enqueue_style( 'font-awesome', $root . '/assets/css/font-awesome.css', array(), '001' );	wp_enqueue_style( 'Playfair+Display+SC', 'http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700italic,700');
	 
}
add_action( 'wp_enqueue_scripts', 'genthemesrocia_scripts_styles' );
 
function genthemesrocia_setup() { 
	
	require( get_template_directory() . '/widgets/Home_Page_AdsTop_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Page_Slider_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Page_News_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Page_Top_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Page_Recent_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Page_Ads_Widget.php' );
	require( get_template_directory() . '/widgets/Home_Page_Blog_Widget.php' );
	
	require( get_template_directory() . '/widgets/Tes_Widget.php' );
	require( get_template_directory() . '/widgets/Ads_Widget.php' );
	require( get_template_directory() . '/widgets/Related_Widget.php' );
	
	load_theme_textdomain( 'genthemesrocia', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );
	register_nav_menu( 'primary', __( 'Primary Menu', 'genthemesrocia' ) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'genthemesrocia_setup' );

function genthemesrocia_widgets_init() {
	
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'genthemesrocia' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'genthemesrocia' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Home Page AdsTop Widget', 'genthemesv1' ),
		'id' => 'home_page_adstop_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Page Slider Widget', 'genthemesv1' ),
		'id' => 'home_page_slider_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Page News Widget', 'genthemesv1' ),
		'id' => 'home_page_news_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Page Top Widget', 'genthemesv1' ),
		'id' => 'home_page_top_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Page Recent Widget', 'genthemesv1' ),
		'id' => 'home_page_recent_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Page Ads Widget', 'genthemesv1' ),
		'id' => 'home_page_ads_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	register_sidebar( array(
		'name' => __( 'Home Page Blog Widget', 'genthemesv1' ),
		'id' => 'home_page_blog_widget',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'genthemesv1' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
	
	register_sidebar( array(
		'name' => __( 'Sidebar First', 'genthemesrocia' ),
		'id' => 'sidebar-first',
		'description' => __( 'tes-lagi-ah', 'genthemesrocia' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',) );
}
add_action( 'widgets_init', 'genthemesrocia_widgets_init' );
if ( ! function_exists( 'genthemesrocia_comment' ) ) :
function genthemesrocia_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'genthemesrocia' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'genthemesrocia' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	  
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">  
              <div class="box-list-image-comment">
               <?php echo get_avatar( $comment, 40 );?>
              </div> 
              <div class="box-list-comment"> 
                <div class="box-detail-comment">
                  <div class="box-date-comment">  
                    <div class="box-edit-comment-left">
                    <cite>
                    <?php echo get_comment_author_link();?>
                    </cite> 
                    <time><?php echo get_comment_date();?></time>
                    </div>
                  </div> 
                  <div class="box-edit-comment">
                    <div class="box-edit-comment-left edit">
                     <?php	edit_comment_link('Edit');?>
                    </div>
                    <div class="box-edit-comment-right reply"> 
                      <?php comment_reply_link( array_merge( $args, array( 'reply_text' => 'Reply', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );?>
                    </div>
                  </div>
                </div>
                <div class="comment-content">
                  <p><?php comment_text(); ?></p>  
                </div><!-- .comment-content --> 
               </div>   
          
        </li><!-- #comment-## -->
        

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'genthemesrocia' ); ?></p>
			<?php endif; ?>  
	<?php
		break;
	endswitch; // end comment_type check
}
endif;
function genthemesrocia_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) )  {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', 'genthemesrocia_enqueue_comments_reply' );
// Setting body background css
function opt_genthemes($var){
	$options = get_option('genthemesfr_options');
	if(isset($options[$var])){
		return $options[$var];
	}
}
function genthemesrocia_class_names($classes) {
	// add 'class-name' to the $classes array
	$classes[] = 'body_genthemes';
	// return the $classes array
	return $classes;
}
add_filter('body_class','genthemesrocia_class_names');
function genthemesrocia_style_body(){
	$options = get_option('genthemesfr_options');
	$body_classs ="";
	if($options["bgradio"] == "bgimage"){
		if($options["attr_bgimage"] == "fixed"){
			$body_classs = "<style>
						body.body_genthemes{
							background: url('".$options['bgimage']."') no-repeat center center fixed; 
						  	-webkit-background-size: cover;
						  	-moz-background-size: cover;
						  	-o-background-size: cover;
						  	background-size: cover; 
						}
						</style>";
		}
	   if($options["attr_bgimage"] == "relative"){
			$body_classs = "<style>
						body.body_genthemes{
							background: url('".$options['bgimage']."') no-repeat scroll center top transparent; 
						  	background-repeat: repeat; /* default */
							background-repeat: repeat-x; /* repeat horizontally */
							background-repeat: repeat-y; /* repeat vertically */
							background-repeat: no-repeat; /* don't tile the image */ 
							background-color: ".$options['bgcolor'].";
						}
						</style>";
		}
	}
	if($options["bgradio"] == "bgpattern"){
		$body_classs = "<style>
						body.body_genthemes{
							background: url('".get_template_directory_uri()."/theme-options/assets/img/pattern/".$options['bgpattern']."');
							background-position: 50% 50%; /* image centered on screen */
							background-position: 50%; /* this also centers on screen */
							background-position: 100px 100px;
							background-position: center;
						}
						</style>";
	}
	if($options["bgradio"] == "bgcolor"){
		$body_classs = "<style>
						body.body_genthemes{
							background: ".$options['bgcolor'].";
						}
						</style>";
	}
    
	
	return print $body_classs;
}
if ( ! function_exists( 'genthemesrocia_content_nav' ) ) :
function genthemesrocia_content_nav( $html_id ) {
	global $wp_query;   
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<ul class="pagination">
		<?php 
				$big = 999999999; // need an unlikely integer 
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) ); 
		?>
	</ul>
	<?php endif;
} 
endif;