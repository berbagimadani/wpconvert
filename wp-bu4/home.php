 
  <?php get_header(); ?>
  <?php if(is_home()){?> 
  <!-- ROW GEN 2 -->
  <!-- [Widget Home Page 1] -->
  <div class="row-gen-2">
    <div class="row">
      <!-- SD 1 -->  
	      <div class="large-7 medium-7 small-12 column">
	      	<?php if ( is_active_sidebar( 'home_page_slider_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_slider_widget' ); ?>
			<?php endif; ?>
	       </div> 
      <!-- //SD 1 -->
      
      <!-- SD 2 --> 
      <div class="large-5 medium-5 small-12 column">
      		<?php if ( is_active_sidebar( 'home_page_news_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_news_widget' ); ?>
			<?php endif; ?>
      </div> 
      <!-- //SD 2 -->
      
    </div>
  </div>
  <!-- //ROW GEN 2 --> 

 <!-- //[Widget Home Page 1] -->


  <!-- ROW GEN 3 -->
  <div class="row-gen-3"> 
    <div class="row"> 
      <div class="large-12 columns">
      	<?php if ( is_active_sidebar( 'home_page_top_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_top_widget' ); ?>
			<?php endif; ?>
      </div>
    </div>    
  </div>
  <!-- //ROW GEN 3 --> 

  <!-- ROW GEN 4 -->
  <div class="row-gen-3">
    <div class="row">
      <div class="large-12 columns">
        <hr>
      </div> 
      
      	<?php if ( is_active_sidebar( 'home_page_recent_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_recent_widget' ); ?>
			<?php endif; ?>
      
      	<?php if ( is_active_sidebar( 'home_page_ads_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_ads_widget' ); ?>
			<?php endif; ?>
      
      	<?php if ( is_active_sidebar( 'home_page_blog_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_blog_widget' ); ?>
			<?php endif; ?>
      
    </div>
  </div>
  <!-- //ROW GEN 4 -->
  
  </div>
  </div>
  <!-- //WIDE --> 
  <?php } ?>  
  <?php get_footer(); ?>
