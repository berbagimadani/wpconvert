<?php if ( is_single() ) : ?>
 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  
  <div class="row-gen-2">
    <div class="row">
      <div class="large-12 column mar-top-right-30">
        <ul class="breadcrumbs">
		<?php 
		if (!is_home()) { 
			if (is_category() || is_single()) {
			echo  the_category(' ');
			echo '<li>'.get_the_title().'</li>';
			}	 
		}
		?>
        </ul>
      </div>
      <div class="large-9 medium-9 small-12 column">
        <!-- CONTENT -->
        <div> 
          <h3><small><?php the_title();?></small></h3> 
          <ul class="large-block-grid-1 medium-block-grid-1 small-block-grid-1">
            <li>
              
				<?php 
				if ( has_post_thumbnail()) {
					echo get_the_post_thumbnail(get_the_ID(), 'full');
				} 
				?>
				
              <?php echo the_content();?>
              <div class="single-tag">
            	<h5>Tags List</h5>
	            <span class="tags">
	            <?php the_tags('',','); ?>
	            </span>
	         </div>  
	         <div data="<?php echo the_permalink(); ?>">FB SAMPLE</div>
            </li> 
          </ul> 
        </div>
        <!-- //CONTENT -->

        <div class="panel-ads"> 
          <img src="img/adsense728x90.gif" />
        </div> 
        <!-- TAB -->
        <dl class="tabs vertical" data-tab>
          <dd class="active"><a href="#panel1a">Item Details</a></dd>
          <dd><a href="#panel2a">Download</a></dd> 
        </dl>
        <div class="tabs-content vertical">
          <div class="content active" id="panel1a">
            <div class="panel">
              <div class="large-4 column">
                <strong>Category</strong>
              </div>
              <div class="large-8 column"> 
                <medium>&nbsp;: <?php echo the_category(' '); ?></medium>
              </div>
              <br>
              <hr>
              <div class="large-4 column">
                <strong>Viewed</strong>
              </div>
              <div class="large-8 column"> 
                <medium>&nbsp;: <span class="label">120</span> View</medium>
              </div>
              <br>
              <hr>
              <div class="large-4 column">
                <strong>File Size</strong> 
              </div>
              <div class="large-8 column"> 
                <medium>&nbsp;: Quality Bleach Wallpaper</medium>
              </div>
              <br>
              <hr>
              <div class="large-4 column">
                <strong>Resolusi</strong>
              </div>
              <div class="large-8 column"> 
                <medium>&nbsp;: <span class="label">1220</span> x <span class="label">1220</span> <span class="label">PX</span></medium>
              </div>
              <br>
              <hr>
              <div class="large-4 column">
                <strong>License</strong>
              </div>
              <div class="large-8 column"> 
                <medium>&nbsp;: GPL</medium>
              </div>
              <br>
              <hr>
              <div class="large-4 column">
                <strong>Total Download</strong>
              </div>
              <div class="large-8 column"> 
                <medium>&nbsp;: <span class="label">120</span></medium>
              </div>
              <br>
              <hr>
            </div>
          </div>
          <div class="content" id="panel2a">
            <div class="panel">
              <?php echo get_the_post_thumbnail(get_the_ID(), 'large');?>
              <div class="align-center mar-top-10"><a href="#" class="button">Download</a></div>
            </div>
          </div> 
        </div>
        <!-- END TAB --> 
        <div class="large-12 medium-12 small-12 column">
        	<?php comments_template( '', true); ?>
       	</div>
    </div>
     <article>
    <?php endif;?> 
     