<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Home_Page_Slider_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Home_Page_Slider_Widget';   
	public $col = '';
	 
	function Home_Page_Slider_Widget() {
		$widget_ops = array( 'classname' => 'Home_Page_Slider_Widget', 'description' => __( 'Use this widget to list your recent Aside, Status, Quote, and Link posts', 'genthemesv1' ) );
		$this->WP_Widget( $this->slugwidget, __('Home_Page_Slider_Widget', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP );
	
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( '', 'genthemesv1' ) : $instance['title'], $instance, $this->id_base);
		$type = apply_filters( 'widget_type', empty( $instance['type'] ) ? __( '', 'genthemesv1' ) : $instance['type'], $instance, $this->id_base);
		
		if ( ! isset( $instance['number'] ) )
			$instance['number'] = '10';
		if ( ! $number = absint( $instance['number'] ) )
 			$number = 10;
 			
 		if ( ! isset( $instance['coloumn'] ) )
			$instance['coloumn'] = $this->col;

		if ( ! $coloumn = absint( $instance['coloumn'] ) )
 			$coloumn = $this->col;	
  
 			  ?> 
				 
	        <!-- [WG LOOP SLIDER,HOME] --> 
	        <div class="mar-top-left-50">
	          <ul class="example-orbit" data-orbit>
	           <?php
					$query = array(
						'post_type' => $this->post_type,
						 $this->taxonomy =>  $type, 
						'order' => 'DESC',
						'posts_per_page' => $number
						);
						$pageposts = new WP_Query($query); 
						while ( $pageposts->have_posts() ) :
								$pageposts->the_post();
								$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
								$featured_image = $featured_image_array[0];
						?>
	            <li>
	              <a href="<?php echo get_permalink();?>">
	              	<?php 
						$default_attr = array( 
						'class'	=> "attachment",
						'alt'	=> trim(strip_tags(get_the_excerpt())),
						'title'	=> trim(strip_tags( get_the_title())),
						);
						?>
	              	<?php 
							if ( has_post_thumbnail()) {
							   echo get_the_post_thumbnail(get_the_ID(), array(647,400), $default_attr); 
							}
							 else{
							 	echo '<img src="'.opt_genthemes('thumbnail').'">';
							 }
							?> 
	              </a>
	              <div class="orbit-caption">
	               <?php echo get_the_content();?>
	               <h5 class="datetime"><small><?php echo get_the_author_meta('nickname');?></small></h5>
	               <h5 class="datetime"><small><?php echo get_the_modified_date()?></small></h5>
	              </div> 
	            </li> 
	            <?php endwhile; ?>
	          </ul> 
	        </div>
	        <!-- [WG LOOP SLIDER,HOME] --> 
	       
 			<?php

			echo $after_widget;

			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata();

			// end check for ephemeral posts
		 

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['type'] = strip_tags( $new_instance['type'] );
		$instance['coloumn'] = (int) $new_instance['coloumn'];
		$instance['number'] = (int) $new_instance['number'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {
		$title = isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$type = isset( $instance['type']) ? esc_attr( $instance['type'] ) : ''; 
		$coloumn = isset( $instance['coloumn']) ? absint( $instance['coloumn'] ) : 7;
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 10;
?>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'genthemesv1' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>"><?php _e( 'Category:', 'genthemesv1' ); ?></label>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'type' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'type' ) ); ?>" >
			<?php 
				$terms = get_terms('category');
				$count = count($terms);
				if ( $count > 0 ){ 
				     foreach ( $terms as $term ) {
				       //echo "<li>" . $term->slug . "</li>";
				       ?>
				       <option value="<?php echo $term->slug ?>" <?php if($type == $term->slug) echo "selected";?>>
				       <?php echo $term->slug; ?>
				       </option> 
				        <?php
				        
				     } 
				}
			?>
			</select>
			</p>
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'genthemesv1' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
		<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Home_Page_Slider_Widget");') );

function Home_Page_Slider_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Home_Page_Slider_Widget_scripts');