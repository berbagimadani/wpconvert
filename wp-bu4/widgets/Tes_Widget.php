<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Tes_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Tes_Widget'; 
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function Tes_Widget() {
		$widget_ops = array( 'classname' => 'Tes_Widget', 'description' => __( 'Use this widget to list your recent Aside, Status, Quote, and Link posts', 'genthemesv1' ) );
		$this->WP_Widget('Tes_Widget', __('Tes_Widget', 'genthemesv1' ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP);
		 
		
		$image_link1 = apply_filters( 'widget_image_link1', empty($instance['image_link1'] ) ? __('', 'genthemesv1') : $instance['image_link1'], $instance, $this->id_base);
		$title_post = apply_filters( 'widget_title_post', empty($instance['title_post'] ) ? __('', 'genthemesv1') : $instance['title_post'], $instance, $this->id_base);
		
		$title = apply_filters( 'widget_title', empty($instance['title'] ) ? __('', 'genthemesv1') : $instance['title'], $instance, $this->id_base);
		$url_image_link1 = apply_filters( 'widget_url_image_link1', empty($instance['url_image_link1'] ) ? __('', 'genthemesv1') : $instance['url_image_link1'], $instance, $this->id_base);
		 
 			  ?> 
				 
      		 
        <div class="block-side-1"> 
          <div class="align-center"><h3><small><?php echo html_entity_decode($title); ?></small></h3></div> 
          <ul class="large-block-grid-1 medium-block-grid-1 small-block-grid-1">
            <li>
              <a href="<?php echo $url_image_link1; ?>" class="th">
              <img src="<?php echo $image_link1; ?>">
              </a>
              <div class="align-center">
                <h3><a href="#"><small><?php echo $title_post; ?></small></a></h3> 
              </div>
            </li> 
          </ul> 
        </div>
       
      
 			<?php

			echo $after_widget;

			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata();

			// end check for ephemeral posts

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;  
		
		$instance['image_link1'] = strip_tags( $new_instance['image_link1']);
		$instance['title_post'] = strip_tags( $new_instance['title_post']);
		
		$instance['title'] = esc_html( $new_instance['title']);
		$instance['url_image_link1'] = esc_html( $new_instance['url_image_link1']);
		
	
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {  
		
		$title = isset( $instance['title']) ? esc_html( $instance['title'] ) : '';
		$url_image_link1 = isset( $instance['url_image_link1']) ? esc_html( $instance['url_image_link1'] ) : '';
		$image_link1 = isset( $instance['image_link1']) ? esc_attr( $instance['image_link1'] ) : '';
		$title_post = isset( $instance['title_post']) ? esc_attr( $instance['title_post'] ) : '';
	?>
		 
				<p><label for="<?php echo esc_attr($this->get_field_id( 'title' ) ); ?>"><?php _e( 'title:', 'genthemesv1'); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr($title); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'url_image_link1' ) ); ?>"><?php _e( 'url_image_link1:', 'genthemesv1'); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'url_image_link1' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'url_image_link1' ) ); ?>" type="text" value="<?php echo esc_attr($url_image_link1); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'image_link1' ) ); ?>"><?php _e( 'image_link1:', 'genthemesv1'); ?></label></p>
		<input type="text" class="widefat image_link1" id="<?php echo esc_attr($this->get_field_id( 'image_link1' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'image_link1' ) ); ?>" value="<?php echo esc_attr($image_link1); ?>"/>	
			<div class="image_link1">
			<?php 
			if(!empty($image_link1)){
			echo '<img src="'.$image_link1.'" style="width:100%">';
			}
			?>
			</div>
			<input type="button" id="select-image_link1" class="upload_image_button button" value="Select Image" name="image_link1">
		<p><label for="<?php echo esc_attr($this->get_field_id( 'title_post' ) ); ?>"><?php _e( 'title_post:', 'genthemesv1'); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title_post' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'title_post' ) ); ?>" type="text" value="<?php echo esc_attr($title_post); ?>"/></p>

		 
	<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Tes_Widget");') );

function Tes_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Tes_Widget_scripts');