<?php 
/**
* Gen Themes Display.
* @package WordPress 
* @subpackage Genthemes V1
* @since genthemes v1
* @web genthemes.net
* @email genthemes@gmail.com
*/
?>
<!doctype html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<html class="no-js" lang="en">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Favicons -->
	<link rel="shortcut icon" href="<?php echo opt_genthemes('favicon');?>" >
	<link rel="apple-touch-icon" href="<?php echo opt_genthemes('favicon');?>" >
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    
    <function:genthemesrocia>  
    <link rel="stylesheet" css="001;foundation;/assets/css/foundation.css" href="css/foundation.css" />
    <link rel="stylesheet" css="001;styles;/assets/css/styles.css" href="css/styles.css" />
    <link rel="stylesheet" css="001;menu;/assets/css/menu.css" href="css/menu.css" />
    <link rel="stylesheet" css="001;hover;/assets/css/hover.css" href="css/hover.css" />  
    <link rel="stylesheet" css="001;font-awesome;/assets/css/font-awesome.css" href="css/hover.css" href="css/font-awesome.css">
    <link rel='stylesheet' css="url;Playfair+Display+SC;http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700italic,700" type='text/css' href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700italic,700'>
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700'>
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Duru+Sans' >
    </function:>
    
  <?php wp_head(); ?> 
  <?php $root = get_template_directory_uri(); ?> 
  <?php genthemesrocia_style_body();?>
  <?php echo opt_genthemes('google-analytic');?>
  </head>
  <body <?php body_class('home blog'); ?>>
  
  <!-- WIDE -->
  <div class="<?php $layout_web = opt_genthemes('layout_web'); if(empty($layout_web)) { echo 'row-gen-wide'; } else { echo $layout_web; }?>" >
  <div class=""> 
  <!-- HEADER -->
  <div class="row">
    <div class="mar-top-10">
      <div class="large-2 medium-2 small-12 columns">
        <div class="left">
	       <a href="<?php echo get_permalink('logo') ?>" ><img src="<?php echo opt_genthemes('logo');?>"></a> 
        </div> 
      </div>
      <div class="large-10 medium-10 small-12 columns">
        <div class="right">
        	<?php if ( is_active_sidebar( 'home_page_adstop_widget' ) ) : ?>
				<?php dynamic_sidebar( 'home_page_adstop_widget' ); ?>
			<?php endif; ?> 
        </div>
      </div>
    </div>
  </div>
  <!-- //HEADER -->

  <!-- ROW GEN 1 -->
  <div class="row">
    <div class="mar-top-10">
      <div class="large-12 medium-12 small-12 column">
        <div class="row-gen-1">
          <div class="large-12 medium-12 small-12 column">
            <!-- MENU -->
            <div class="block-menu"> 
              <nav class="top-bar" data-topbar>  
                <section class="top-bar-section">
                  <?php foundation_head_bar();?>
                </section>
              </nav>
            </div>
            <!-- //MENU -->
          </div>
        </div>
      </div>
    </div> 
  </div> 
  <!-- //ROW GEN 1 -->
