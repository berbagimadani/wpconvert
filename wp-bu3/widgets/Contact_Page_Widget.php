<?php
/**
 * Makes a custom Widget for displaying Aside, Link, Status, and Quote Posts available with genthemes
 *
 * Learn more: http://codex.wordpress.org/Widgets_API#Developing_Widgets
 *
 * @package WordPress
 * @subpackage genthemes
 * @since genthemes.01
 */
class Contact_Page_Widget extends WP_Widget {
	/**
	 * Constructor
	 *
	 * @return void
	 **/
	 
	public $taxonomy  = 'category_name';
	public $post_type = 'post';
	
	public $slugwidget = 'Contact_Page_Widget';
	public $namewidget = 'Contact_Page_Widget';
	public $titlewidget = 'Contact_Page_Widget';
	public $versionwidget = 'genthemesv1';
	public $col = '';
	 
	function Contact_Page_Widget() {
		$widget_ops = array( 'classname' => $this->slugwidget, 'description' => __( 'Use this widget to list your recent Aside, Status, Quote, and Link posts', 'genthemesv1' ) );
		$this->WP_Widget( $this->slugwidget, __($this->namewidget, $this->versionwidget ), $widget_ops );
		$this->alt_option_name = $this->slugwidget;

		add_action( 'save_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array(&$this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array(&$this, 'flush_widget_cache' ) );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array An array of standard parameters for widgets in this theme
	 * @param array An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		$cache = wp_cache_get( $this->slugwidget, 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = null;

		if ( isset( $cache[$args['widget_id']] ) ) {
			echo $cache[$args['widget_id']];
			return;
		}

		ob_start();
		extract( $args, EXTR_SKIP );
		 
		
				$label_address = apply_filters( 'widget_label_address', empty($instance['label_address'] ) ? __('', $this->versionwidget ) : $instance['label_address'], $instance, $this->id_base);		$label_head_office = apply_filters( 'widget_label_head_office', empty($instance['label_head_office'] ) ? __('', $this->versionwidget ) : $instance['label_head_office'], $instance, $this->id_base);		$head_office = apply_filters( 'widget_head_office', empty($instance['head_office'] ) ? __('', $this->versionwidget ) : $instance['head_office'], $instance, $this->id_base);		$label_phone = apply_filters( 'widget_label_phone', empty($instance['label_phone'] ) ? __('', $this->versionwidget ) : $instance['label_phone'], $instance, $this->id_base);		$phone = apply_filters( 'widget_phone', empty($instance['phone'] ) ? __('', $this->versionwidget ) : $instance['phone'], $instance, $this->id_base);		$label_email = apply_filters( 'widget_label_email', empty($instance['label_email'] ) ? __('', $this->versionwidget ) : $instance['label_email'], $instance, $this->id_base);		$email = apply_filters( 'widget_email', empty($instance['email'] ) ? __('', $this->versionwidget ) : $instance['email'], $instance, $this->id_base);		$label_sosmed = apply_filters( 'widget_label_sosmed', empty($instance['label_sosmed'] ) ? __('', $this->versionwidget ) : $instance['label_sosmed'], $instance, $this->id_base);		$sosmed = apply_filters( 'widget_sosmed', empty($instance['sosmed'] ) ? __('', $this->versionwidget ) : $instance['sosmed'], $instance, $this->id_base);
		 
 			  ?> 
				 
      		
          <div style="padding-top:20px; padding-left:30px; text-align:left; float:left">
            <h2><?php echo html_entity_decode($label_address); ?></h2>
            <div class="head-contact"><strong><?php echo html_entity_decode($label_head_office); ?></strong></div>
            <p class="title-contact">
             <?php echo html_entity_decode($head_office); ?>
            </p> 
            <div class="head-contact"><strong><?php echo html_entity_decode($label_phone); ?></strong></div>
            <p class="title-contact">
             <?php echo html_entity_decode($phone); ?>
            </p> 
            <div class="head-contact"><strong><?php echo html_entity_decode($label_email); ?></strong></div>
            <p class="title-contact">
             <?php echo html_entity_decode($email); ?>
            </p> 
            <div class="head-contact"><strong><?php echo html_entity_decode($label_sosmed); ?></strong></div>
            <p class="title-contact">
            <?php echo html_entity_decode($sosmed); ?>
            </p> 
          </div>
          
      
 			<?php

			echo $after_widget;

			// Reset the post globals as this query will have stomped on it
			wp_reset_postdata();

			// end check for ephemeral posts
		 

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set($this->slugwidget, $cache, 'widget');
	}

	/**
	 * Deals with the settings when they are saved by the admin. Here is
	 * where any validation should be dealt with.
	 **/
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;  
		
				$instance['label_address'] = esc_html( $new_instance['label_address']);		$instance['label_head_office'] = esc_html( $new_instance['label_head_office']);		$instance['head_office'] = esc_html( $new_instance['head_office']);		$instance['label_phone'] = esc_html( $new_instance['label_phone']);		$instance['phone'] = esc_html( $new_instance['phone']);		$instance['label_email'] = esc_html( $new_instance['label_email']);		$instance['email'] = esc_html( $new_instance['email']);		$instance['label_sosmed'] = esc_html( $new_instance['label_sosmed']);		$instance['sosmed'] = esc_html( $new_instance['sosmed']);
		
	
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset( $alloptions[$this->slugwidget] ) )
			delete_option( $this->slugwidget );

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete( $this->slugwidget, 'widget' );
	}

	/**
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 **/
	function form( $instance ) {  
				$label_address = isset( $instance['label_address']) ? esc_html( $instance['label_address'] ) : '';		$label_head_office = isset( $instance['label_head_office']) ? esc_html( $instance['label_head_office'] ) : '';		$head_office = isset( $instance['head_office']) ? esc_html( $instance['head_office'] ) : '';		$label_phone = isset( $instance['label_phone']) ? esc_html( $instance['label_phone'] ) : '';		$phone = isset( $instance['phone']) ? esc_html( $instance['phone'] ) : '';		$label_email = isset( $instance['label_email']) ? esc_html( $instance['label_email'] ) : '';		$email = isset( $instance['email']) ? esc_html( $instance['email'] ) : '';		$label_sosmed = isset( $instance['label_sosmed']) ? esc_html( $instance['label_sosmed'] ) : '';		$sosmed = isset( $instance['sosmed']) ? esc_html( $instance['sosmed'] ) : '';
	?>
		 
				<p><label for="<?php echo esc_attr($this->get_field_id( 'label_address' ) ); ?>"><?php _e( 'label_address:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'label_address' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'label_address' ) ); ?>" type="text" value="<?php echo esc_attr($label_address); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'label_head_office' ) ); ?>"><?php _e( 'label_head_office:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'label_head_office' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'label_head_office' ) ); ?>" type="text" value="<?php echo esc_attr($label_head_office); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'head_office' ) ); ?>"><?php _e( 'head_office:', $this->versionwidget ); ?></label></p>
		<p><textarea rows="8" cols="20" class="widefat" id="<?php echo esc_attr($this->get_field_id( 'head_office' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'head_office' ) ); ?>"><?php echo esc_attr($head_office); ?></textarea></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'label_phone' ) ); ?>"><?php _e( 'label_phone:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'label_phone' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'label_phone' ) ); ?>" type="text" value="<?php echo esc_attr($label_phone); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'phone' ) ); ?>"><?php _e( 'phone:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'phone' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'phone' ) ); ?>" type="text" value="<?php echo esc_attr($phone); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'label_email' ) ); ?>"><?php _e( 'label_email:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'label_email' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'label_email' ) ); ?>" type="text" value="<?php echo esc_attr($label_email); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'email' ) ); ?>"><?php _e( 'email:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'email' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'email' ) ); ?>" type="text" value="<?php echo esc_attr($email); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'label_sosmed' ) ); ?>"><?php _e( 'label_sosmed:', $this->versionwidget ); ?></label></p>
		<p><input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'label_sosmed' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'label_sosmed' ) ); ?>" type="text" value="<?php echo esc_attr($label_sosmed); ?>"/></p>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'sosmed' ) ); ?>"><?php _e( 'sosmed:', $this->versionwidget ); ?></label></p>
		<p><textarea rows="8" cols="20" class="widefat" id="<?php echo esc_attr($this->get_field_id( 'sosmed' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'sosmed' ) ); ?>"><?php echo esc_attr($sosmed); ?></textarea></p>

		 
	<?php
	}
}
// init the widget
add_action( 'widgets_init', create_function('', 'return register_widget("Contact_Page_Widget");') );

function Contact_Page_Widget_scripts() {
	global $pagenow;
	if( $pagenow == 'widgets.php' ) {
	wp_enqueue_script('jquery');
	wp_enqueue_script('media-upload');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/js/media_upload.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload'); }
}
add_action('admin_enqueue_scripts', 'Contact_Page_Widget_scripts');