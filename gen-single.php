<?php 
include("ClassFolderName.php"); 
?>
<?php
$homepage = file_get_contents('theme/single.html');


get_content('display',$homepage);

function get_content( $tag , $content )
{
    /* CREATE COMMENT*/ 
    $comment = file_get_contents('templates/comments.php');
    
	$tag_com ="gt:comment"; 
    preg_match_all("/<".$tag_com."[^>]*>(.*?)<\/$tag_com>/si", $content, $matches_com);
     
	foreach ($matches_com[1] as $key => $value_com) {  
     	$vals_com .= $value_com;  
    }
    
    $tag = "comment_list";
    preg_match_all("/{".$tag.":\"*(.*?)\}/si", $vals_com, $matches);
    $str = $matches[1];
	foreach ($str as $key => $value_s) {  
 		$exp_variable = explode(";", $value_s); 
		
 		$ol .="\r"."		<$exp_variable[2] class=\"$exp_variable[0]\">
		<?php wp_list_comments( array( 'callback' => '$exp_variable[1]_comment', 'style' => '$exp_variable[2]' ) ); ?>
		</$exp_variable[2]>";
 		$olrep .= "{comment_list:$exp_variable[0];$exp_variable[1];$exp_variable[2]}";
	}
	
	$tag_form = "form_title";
    preg_match_all("/{".$tag_form.":\"*(.*?)\}/si", $content, $matches_form);
    $str_form = $matches_form[1];
	foreach ($str_form as $key => $value_form) {  
 		$exp_variable = explode(";", $value_form); 
 		
 		if($exp_variable[0] == '1'){
 			$formtitle1 .= $exp_variable[1];
 			$formtitlesearch1 .= "{form_title$exp_variable[0]}";
 		}
		if($exp_variable[0] == '2'){
 			$formtitle2 .= $exp_variable[1];
 			$formtitlesearch2 .= "{form_title$exp_variable[0]}";
 		}
		if($exp_variable[0] == 'author'){ 
			$sea = array('[req_start]',
						 '[req_end]',
			             '[value]',
			             '[req]');
			$rep = array("'.( \$req ? '",
			             "' : '' ).'",
						 "'.esc_attr(  \$commenter['comment_author'] ).'",
			             "'.\$aria_req.'");
    		$con = str_replace($sea, $rep,$exp_variable[1]);			  
 			$formtitle_author .= $con;
 			
 			$formtitlesearch_author .= "{form_title_$exp_variable[0]}"; 
 		}
		 
		if($exp_variable[0] == 'email'){ 
			$sea = array('[req_start]',
						 '[req_end]',
			             '[value]',
			             '[req]');
			$rep = array("'.( \$req ? '",
			             "' : '' ).'",
						 "'.esc_attr(  \$commenter['comment_author_email'] ).'",
			             "'.\$aria_req.'");
    		$con = str_replace($sea, $rep,$exp_variable[1]);			  
 			$formtitle_email .= $con;
 			
 			$formtitlesearch_email .= "{form_title_$exp_variable[0]}"; 
		}
		
		if($exp_variable[0] == 'url'){ 
			$sea = array('[req_start]',
						 '[req_end]',
			             '[value]',
			             '[req]');
			$rep = array("'.( \$req ? '",
			             "' : '' ).'",
						 "'.esc_attr(  \$commenter['comment_author_email'] ).'",
			             "'.\$aria_req.'");
    		$con = str_replace($sea, $rep,$exp_variable[1]);			  
 			$formtitle_url .= $con;
 			
 			$formtitlesearch_url .= "{form_title_$exp_variable[0]}"; 
		}
		
		if($exp_variable[0] == 'button'){ 
			/*
			$sea = array('[id_button]',
						 '[label_button]');
			*/ 
			$tag_button = "button";
		    preg_match_all("/<".$tag_button.":\"*(.*?)\>/si", $exp_variable[1], $matches_button);
		    $str_button = $matches_button[1];
			foreach ($str_button as $key => $value_button) {  
				$isi_button .= $value_button.";"; 
 			}
 			$exp_variable_button = explode(";", $isi_button); 
 			 
 			
 			$label_button .= $exp_variable_button[1];
 			$id_button .= $exp_variable_button[0];
		} 
	} 
	 
	$search_com1 = array('{title_comment}',
    					  $olrep);
    $replace_com1 = array("<?php
    		printf( _n( 'One thought on &ldquo;%2$s&rdquo;', '%1$s comments on &ldquo;%2$s&rdquo;', get_comments_number(), 'genthemesv1' ),
			number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>",
    		$ol);
    		
    $com_content1 = str_replace($search_com1, $replace_com1,$vals_com);
    
    $search_com = array('{title_comment}',
                         $formtitlesearch1,
                         $formtitlesearch2,
                         $formtitlesearch_author,
                         $formtitlesearch_email,
                         $formtitlesearch_url,
                         '{id_button}',
                         '{label_button}');
    $replace_com = array($com_content1,
    					 $formtitle1,
    					 $formtitle2,
    					 $formtitle_author,
    					 $formtitle_email,
    					 $formtitle_url,
    					 $id_button,
    					 $label_button);
    $com_content = str_replace($search_com, $replace_com, $comment);
     
    $file_comment= FolderName::get('themeFolder')."/comments.php";
    writefile($file_comment,$com_content); 
    
    
    $tes = file_get_contents(FolderName::get('themeFolder').'/functions_remove.php');
    
	$tag_com2 ="gtcomment:";
    $filename_com2 = str_replace('gtcomment:','',$tag_com2);
    preg_match_all("/<".$tag_com2."[^>]*>(.*?)<\/$tag_com2>/si", $content, $matches_com2);
     
	foreach ($matches_com2[1] as $key => $value_com2) {  
     	$vals_com2 .= $value_com2;  
    }
    
	$tag_class = "comment_li";
    preg_match_all("/{".$tag_class.":\"*(.*?)\}/si", $vals_com2, $matches_class);
    $str_class = $matches_class[1];
	foreach ($str_class as $key => $value_class) {  
 		$exp_variable = explode(";", $value_class); 
 		
		if($exp_variable[0] == "class"){
 			$class .= "<?php comment_class(); ?>";
	 		$classrep .= "{comment_li:$exp_variable[0];$exp_variable[1]}";
 		} 
		if($exp_variable[0] == "id"){
 			$id .= "li-comment-<?php comment_ID(); ?>";
	 		$idrep .= "{comment_li:$exp_variable[0];$exp_variable[1]}";
 		} 
		if($exp_variable[0] == "avatar"){
 			$avatar .= "<?php echo get_avatar( \$comment, $exp_variable[1] );?>";
	 		$avatarrep .= "{comment_li:$exp_variable[0];$exp_variable[1]}";
 		}
		if($exp_variable[0] == "author_link"){
 			$author_link .= "<?php echo get_comment_author_link();?>";
	 		$author_linkrep .= "{comment_li:$exp_variable[0]}";
 		}
		if($exp_variable[0] == "datetime"){
 			$datetime .= "<?php echo get_comment_date();?>";
	 		$datetimerep .= "{comment_li:$exp_variable[0]}";
 		}
		if($exp_variable[0] == "edit"){
 			$edit .= "<?php	edit_comment_link('Edit');?>";
	 		$editrep .= "{comment_li:$exp_variable[0]}";
 		}
		if($exp_variable[0] == "reply"){
 			$reply .= "<?php comment_reply_link( array_merge( \$args, array( 'reply_text' => 'Reply', 'depth' => \$depth, 'max_depth' => \$args['max_depth'] ) ) );?>";
	 		$replyrep .= "{comment_li:$exp_variable[0]}";
 		}
		if($exp_variable[0] == "comment_text"){
 			$comment_text .= "<?php comment_text(); ?>";
	 		$comment_textrep .= "{comment_li:$exp_variable[0]}";
 		} 
	}
    $search_fun = array( $classrep,
    					  $idrep,
    					  $avatarrep,
    					  $author_linkrep,
    					  $datetimerep,
    					  $editrep,
    					  $replyrep,
    					  $comment_textrep);
    $replace_fun = array(
    		$class,
    		$id,
    		$avatar,
    		$author_link,
    		$datetime,
    		$edit,
    		$reply,
    		$comment_text);
    		
    $fun_content1 = str_replace($search_fun, $replace_fun,$vals_com2);
    		
    $search_tes = array('<!--{testing}-->');
    $replace_tes = array($fun_content1);
    $tes2 = str_replace($search_tes, $replace_tes,$tes);
    $file_tes= FolderName::get('themeFolder')."/functions.php";
    writefile($file_tes,$tes2);  
    
    /* CREATE CONTENT */ 
    gtContent($content);
    /* CREATE SIDEBAR */
    gtSidebar($content);
    /* CREATE WIDGET */
	$tag_widget ="genthemesid";  
    preg_match_all("/".$tag_widget."=\"*(.*?)\"/si", $content, $matches_widget); 
     
	foreach ($matches_widget[1] as $key => $value_widget) {   
     	if(replaceString($value_widget) == "custome_widget"){
     	 createWidgetsCustome($content,str_replace('custome_widget:','',$value_widget));
     	}
		if(replaceString($value_widget) == "is_active_sidebar"){
     	 createWidgets($content,str_replace('is_active_sidebar:','',$value_widget));
     	}
     	 
    }
}
function createWidgets($content,$val){
	/* CREATE Widget   */ 
    $tag_wg ="gt:$val";
    $filename_wg = str_replace('gt:','',$tag_wg);
    preg_match_all("/<".$tag_wg."[^>]*>(.*?)<\/$tag_wg>/si", $content, $matches_wg);
     
	foreach ($matches_wg[1] as $key => $value) {  
     $vals_wg .= $value; 
    }
	
    /*
    $tag_num = "img_query";
    preg_match_all("/{".$tag_num .":genthemes\"*(.*?)\}/si", $content, $matches_num);
    $str_um = $matches_num[1];
	foreach ($str_um as $key => $value_num) {
		$exp_variable = explode(",", $value_num);
		$ccc .= $value_num[0];
	}*/
     
	    $tag = "img_query";
	    preg_match_all("/{".$tag.":\"*(.*?)\}/si", $content, $matches);
	    $str = $matches[1];
		foreach ($str as $key => $value_s) {
			$exp_variable = explode(",", $value_s); 
			$img_query .= "{img_query:$value_s};genthemes";
			 
			if(is_numeric($exp_variable[0])){ 
				$img_query_var .='<?php 
							$default_attr = array( 
							\'class\'	=> "'.$exp_variable[2].'",
							\'alt\'	=> trim(strip_tags(get_the_excerpt())),
							\'title\'	=> trim(strip_tags( get_the_title())),
							);
							?>
							<?php 
							if ( has_post_thumbnail()) {
							   echo get_the_post_thumbnail(get_the_ID(), array('.$exp_variable[0].','.$exp_variable[1].'), $default_attr); 
							} 
							?>;genthemes';
			}
			else{ 
				$img_query_var .='
							<?php 
							$default_attr = array( 
							\'class\'	=> "'.$exp_variable[1].'",
							\'alt\'	=> trim(strip_tags(get_the_excerpt())),
							\'title\'	=> trim(strip_tags( get_the_title())),
							);
							?>
							<?php 
							if ( has_post_thumbnail()) {
							   echo get_the_post_thumbnail(get_the_ID(), \''.$exp_variable[0].'\', $default_attr); 
							} 
				?>;genthemes';
			}  
		} 
		
		$exp_variable = explode(";genthemes", $img_query);
		$exp_variable_2 = explode(";genthemes", $img_query_var);
		 
    $model = file_get_contents('templates/widgets.php');
    
    $query = "<?php
					\$query = array(
						'post_type' => \$this->post_type,
						 \$this->taxonomy =>  \$type, 
						'order' => 'DESC',
						'posts_per_page' => \$number
						);
						\$pageposts = new WP_Query(\$query); 
						while ( \$pageposts->have_posts() ) :
								\$pageposts->the_post();
								\$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'single-post-thumbnail' );
								\$featured_image = \$featured_image_array[0];
						?>";
    $endquery = "<?php endwhile; ?>"; 
    
	$search_m = array('{slugwidget}',
					  '{content}',
					  '{query:widget}',
					  '{/query:widget}',
					  '{post_title}',
					  '{post_content}',
					  '{img-large}',
					  '{permalink}',
					  '{title_head}',
					  '{post_author}',
					  '{post_date}', 
					  '{count_comment}',
					  '{count_view}',
					  '{namewidget}');
	$replace_m = array($val,
					   $vals_wg,
					   $query,
					   $endquery,
					  '<?php echo get_the_title();?>',
					  '<?php echo get_the_content();?>',
					  '<?php echo $featured_image;?>',
					  '<?php echo get_permalink();?>',
					  '<?php echo $title; ?>',
					  '<?php echo get_the_author_meta(\'nickname\');?>',
					  '<?php echo get_the_modified_date()?>', 
					  "<?php comments_popup_link( '0 Comments', '1 Comments', '% Comments', '', '0 Comments'); ?>",
					  '<?php echo view_count(get_the_ID())?>',
					  str_replace('_', ' ', $val),);
	//$img_query_var
	$m_content = str_replace($search_m, $replace_m, $model);
	
	$search_m2 = $exp_variable;
	$replace_m2 = $exp_variable_2;
	$m_content2 = str_replace($search_m2, $replace_m2, $m_content);
	
    $file_wg = FolderName::get('themeFolder')."/widgets/".$val.".php";
    writefile($file_wg,$m_content2); 
    /*//CREATE Widget */
}
function gtContent($content){
	$tag_com ="gt:single"; 
    preg_match_all("/<".$tag_com."[^>]*>(.*?)<\/$tag_com>/si", $content, $matches_com);
     
	foreach ($matches_com[1] as $key => $value_com) {  
     	$vals_com .= $value_com;  
    }
    
    $tag = "block";
    preg_match_all("/{".$tag.":\"*(.*?)\}/si", $vals_com, $matches);
    $str = $matches[1];
	foreach ($str as $key => $value_s) {
		$exp_variable = explode(";", $value_s);  
		if($exp_variable[0] == "img"){
			$dsea .= "$tag:$exp_variable[0];$exp_variable[1]";
			$drep .= "
				<?php 
				if ( has_post_thumbnail()) {
					echo get_the_post_thumbnail(get_the_ID(), '$exp_variable[1]');
				} 
				?>
				";
		}
		if($exp_variable[0] == "tags"){
			$esea .= "$tag:$exp_variable[0];$exp_variable[1]";
			$erep .= "<?php the_tags('','$exp_variable[1]'); ?>";
		}
		if($exp_variable[0] == "img-display"){
			$fsea .= "$tag:$exp_variable[0];$exp_variable[1]";
			$frep .= "<?php echo get_the_post_thumbnail(get_the_ID(), '$exp_variable[1]');?>";
		}
		if($exp_variable[0] == "comment_template"){
			$gsea .= "$tag:$exp_variable[0];$exp_variable[1]";
			$grep .= "<?php comments_template( '', $exp_variable[1]); ?>";
		}
	}
     
    
    $search_a = array('{is_single}',
    				  '{close_is_single}',
    				  '{breadcrumbs}',
    				  '{'.$dsea.'}',
    				  '{content}',
    				  '{'.$esea.'}',
    				  '{'.$fsea.'}',
    				  '{category}',
    		          '{url_link}',
    				  '{'.$gsea.'}',
    				  '{title}',
    				  '{cat_name}',
    
    				  '{post_title}',  
					  '{post_author}',
					  '{post_date}',  
					  '{count_comment}',
					  '{count_view}', 
					  '{permalink}',);
    
    
    $replace_a = array('<?php if ( is_single() ) : ?>
 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>',
'<article>
    <?php endif;?>',
"
		<?php 
		if (!is_home()) { 
			if (is_category() || is_single()) {
			echo  the_category(' ');
			echo '<li>'.get_the_title().'</li>';
			}	 
		}
		?>",
		"$drep",
    	"<?php echo the_content();?>",
    	"$erep",
    	"$frep",
    	"<?php echo the_category(' '); ?>",
    	"<?php echo the_permalink(); ?>",
        "$grep",
    	"<?php the_title();?>",
        "<?php \$category = get_the_category(); echo \$category[0]->cat_name;?>",
    	
    	'<?php echo get_the_title();?>',
	    '<?php echo get_the_author_meta(\'nickname\');?>',
	    '<?php echo get_the_modified_date()?>', 
	    "<?php comments_popup_link( '0 Comments', '1 Comments', '% Comments', '', '0 Comments'); ?>",
		'<?php echo view_count(get_the_ID())?>', 
	    '<?php echo get_permalink();?>',);
    		
    $content_a = str_replace($search_a,$replace_a,$vals_com);
    
    print $content_a;
    
	$tag_related = "query";
    preg_match_all("/{".$tag_related.":\"*(.*?)\}/si", $content_a, $matches_related);
    $str_related = $matches_related[1];
	foreach ($str_related as $key => $value_related) {
		 $exp_variable = explode(",", $value_related); 
		 if( $exp_variable[0] == "related"){
		 	print $value_related;
		 	if($exp_variable[1] != ''){
		 		$page = "'posts_per_page' => '$exp_variable[1]'";
		 	}
		 	$s .= $value_related;
		 	$q .= '<?php
		 			$category = get_the_category();  
					$query = array(
						\'post_type\' => \'post\',
						\'category_name\'=>  $category[0]->slug, 
						\'order\' => \'DESC\', 
						\'post__not_in\' => array($post->ID),
						'.$page.'
						);
						$pageposts = new WP_Query($query); 
						while ( $pageposts->have_posts() ) :
								$pageposts->the_post();
								$featured_image_array = wp_get_attachment_image_src( get_post_thumbnail_id(), \'single-post-thumbnail\' );
								$featured_image = $featured_image_array[0];
					?>';
		 }
	}
	$search_related = array("{query:$s}","{/query:$s}");
	$replace_related = array("$q",'<?php endwhile;?>'); 
	$content_b = str_replace($search_related,$replace_related,$content_a);
	
	$tag_img = "img_query";
	    preg_match_all("/{".$tag_img.":\"*(.*?)\}/si", $content_b, $matches_img);
	    $str = $matches_img[1];
		foreach ($str as $key => $value_s) {
			$exp_variable = explode(",", $value_s); 
			$img_query .= "{img_query:$value_s};genthemes";
			 
			if(is_numeric($exp_variable[0])){ 
				$img_query_var .='<?php 
								$default_attr = array( 
								\'class\'	=> "'.$exp_variable[2].'",
								\'alt\'	=> trim(strip_tags(get_the_excerpt())),
								\'title\'	=> trim(strip_tags( get_the_title())),
								);
								?>
								<?php 
								if ( has_post_thumbnail()) {
								   echo get_the_post_thumbnail(get_the_ID(), array('.$exp_variable[0].','.$exp_variable[1].'), $default_attr); 
								} 
								?>;genthemes';
			}
			else{ 
				$img_query_var .='<?php 
								$default_attr = array( 
								\'class\'	=> "'.$exp_variable[1].'",
								\'alt\'	=> trim(strip_tags(get_the_excerpt())),
								\'title\'	=> trim(strip_tags( get_the_title())),
								);
								?>
								<?php 
								if ( has_post_thumbnail()) {
								   echo get_the_post_thumbnail(get_the_ID(), \''.$exp_variable[0].'\', $default_attr); 
								} 
								?>;genthemes';
			}  
		} 
		
	$exp_variable = explode(";genthemes", $img_query);
	$exp_variable_2 = explode(";genthemes", $img_query_var);
	
	$search_m2 = $exp_variable;
	$replace_m2 = $exp_variable_2;
	$m_content2 = str_replace($search_m2, $replace_m2, $content_b);
	
	
	$tag_content = "post_content";
	    preg_match_all("/{".$tag_content.":\"*(.*?)\}/si", $m_content2, $matches_content);
	    $str = $matches_content[1];
		foreach ($str as $key => $value_s) { 
			$post_content .= "{post_content:$value_s};genthemes";
			 
			if(is_numeric($value_s)){ 
				$post_content_var .='<?php echo wp_trim_words( get_the_content(),'.$value_s.' );?>;genthemes';
			}
			else{ 
				$post_content_var .='<?php echo the_content(); ?>;genthemes';
			}  
		} 
		
	$exp_variable_content = explode(";genthemes", $post_content);
	$exp_variable_content_2 = explode(";genthemes", $post_content_var);
	
	$search_m3 = $exp_variable_content;
	$replace_m3 = $exp_variable_content_2;
	$m_content3 = str_replace($search_m3, $replace_m3, $m_content2);
	
    $file_tes= FolderName::get('themeFolder')."/content.php";
    writefile($file_tes,$m_content3); 
    
}
function gtSidebar($content){
	$tag_com ="gt:sidebar"; 
    preg_match_all("/<".$tag_com."[^>]*>(.*?)<\/$tag_com>/si", $content, $matches_com);
     
	foreach ($matches_com[1] as $key => $value_com) {  
     	$vals_com .= $value_com;  
    }
     
    $tag = "block";
    preg_match_all("/{".$tag.":\"*(.*?)\}/si", $vals_com, $matches);
    $str = $matches[1];
	foreach ($str as $key => $value_s) {
		$exp_variable = explode(";", $value_s); 
		$str1 = strSlug($exp_variable[0]);
		$str2 = strSlug($exp_variable[2]);
		$str3 = strSlug($exp_variable[1]);
		$block .= "{block:$exp_variable[0];$exp_variable[1];$exp_variable[2]}".";;";
		$register_sidebar .="\n"."	register_sidebar( array(
		'name' => __( '$exp_variable[0]', '$str2' ),
		'id' => '$str1',
		'description' => __( '$str3', '$str2' ),
		'before_widget' => '<aside id=\"%1\$s\" class=\"widget %2\$s\">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class=\"widget-title\">',
		'after_title' => '</h3>',) );"; 
		
		$d .= "\r"."	<?php if ( is_active_sidebar( '".$str1."' ) ) : ?>
	<?php dynamic_sidebar( '".$str1."' ); ?>
	<?php endif; ?>".";;"; 
		 
	}
	
	$tag = "genthemesid";
    preg_match_all("/".$tag."=\"*(.*?)\"/si", $content, $matches);
    $str = $matches[1]; 
    foreach ($str as $key => $value) {   
    	if(replaceString($value) == "custome_widget"){
    		$strrep = str_replace('custome_widget:','',$value); 
    		$include_file .="\n"."	require( get_template_directory() . '/widgets/$strrep.php' );";
    	}
    	if(replaceString($value) == "is_active_sidebar"){
    		$strrep = str_replace('is_active_sidebar:','',$value); 
    		$include_file .="\n"."	require( get_template_directory() . '/widgets/$strrep.php' );";
    	}
    }
     
	
	$tes = file_get_contents(FolderName::get('themeFolder').'/functions.php');
	
	$search_tes = array('/*{create_sidebar_remove}*/','/*{include_sidebar_remove}*/');
    $replace_tes = array($register_sidebar,$include_file);
    $tes2 = str_replace($search_tes, $replace_tes,$tes);
    $file_tes= FolderName::get('themeFolder')."/functions.php";
    writefile($file_tes,$tes2);

    
    $tag_del ="ch"; 
    preg_match_all("/<".$tag_del."[^>]*>(.*?)<\/$tag_del>/si", $vals_com, $matches_del);
     
	foreach ($matches_del[0] as $key => $value_del) {
		$d4 .= $value_del.'::';
    } 
     
    $ar = explode("::", $d4);
    
    $search_m_2 = $ar;
	$replace_m_2 = array(''); 
    $m_content_2 = str_replace($search_m_2, $replace_m_2, $vals_com);
     
    
    
    $ar_b = explode(";;",$block);
    $ar_c = explode(";;",$d);
    
    $sea = ($ar_b);
    $rep = ($ar_c);
    $final = clearTab(str_replace($sea,$rep,$m_content_2));
    
    $file_sidebar= FolderName::get('themeFolder')."/sidebar.php";
    writefile($file_sidebar,$final);
     
     
}

function clearTab($final){
	$sea = array(' 
       
       
       ','

       ');
	$rep = array('','');
	
	return str_replace($sea,$rep,$final);
}
function createWidgetsCustome($content,$val){
	/* CREATE Widget custome form field */ 
    $tag_wg ="tcustome:$val";
    $filename_wg = str_replace('tcustome:','',$tag_wg);
    preg_match_all("/<".$tag_wg."[^>]*>(.*?)<\/$tag_wg>/si", $content, $matches_wg);
     
	foreach ($matches_wg[1] as $key => $value) {  
     $vals_wg .= $value; 
    }

    
    $tag = "form";
    preg_match_all("/{".$tag.":\"*(.*?)\}/si", $vals_wg, $matches);
    $str = $matches[1];
	foreach ($str as $key => $value_s) {  
		/*$val_form .= "\n"."<p><label for=\"<?php echo esc_attr(\$this->get_field_id( '$value_s' ) ); ?>\"><?php _e( '$value_s:', \$this->versionwidget ); ?></label>";*/
		$exp_variable = explode(",", $value_s); 
		
		if($exp_variable[6] == "link"){
		     $tes .=$exp_variable[1];
		     $tesSearch .= "{form:$exp_variable[0],$exp_variable[1],$exp_variable[2],'',$exp_variable[4],$exp_variable[5],$exp_variable[6]}";
		}
		if($exp_variable[0] == "input"){
			
			if($exp_variable[3] != ''){
				$num = $exp_variable[3];
			}else { $num = "''"; }
			 
			if($exp_variable[4] == "img"){
			$field = "		<input type=\"text\" class=\"widefat $exp_variable[1]\" id=\"<?php echo esc_attr(\$this->get_field_id( '$exp_variable[1]' ) ); ?>\" name=\"<?php echo esc_attr(\$this->get_field_name( '$exp_variable[1]' ) ); ?>\" value=\"<?php echo esc_attr($$exp_variable[1]); ?>\"/>	
			<div class=\"$exp_variable[1]\">
			<?php 
			if(!empty(\$$exp_variable[1])){
			echo '<img src=\"'.$$exp_variable[1].'\" style=\"width:100%\">';
			}
			?>
			</div>
			<input type=\"button\" id=\"select-$exp_variable[1]\" class=\"upload_image_button button\" value=\"Select Image\" name=\"$exp_variable[1]\">"."\n";
			}
			else{
			$field = "		<p><input class=\"widefat\" id=\"<?php echo esc_attr(\$this->get_field_id( '$exp_variable[1]' ) ); ?>\" name=\"<?php echo esc_attr(\$this->get_field_name( '$exp_variable[1]' ) ); ?>\" type=\"text\" value=\"<?php echo esc_attr($$exp_variable[1]); ?>\"/></p>"."\n";
			}
		}
		elseif($exp_variable[0] == "textarea"){
			
			if($exp_variable[3] != ''){
				$num = $exp_variable[3];
			}else { $num = "''"; }
			
			$field = "		<p><textarea rows=\"8\" cols=\"20\" class=\"widefat\" id=\"<?php echo esc_attr(\$this->get_field_id( '$exp_variable[1]' ) ); ?>\" name=\"<?php echo esc_attr(\$this->get_field_name( '$exp_variable[1]' ) ); ?>\"><?php echo esc_attr($$exp_variable[1]); ?></textarea></p>"."\n";
		} 
		
		if($exp_variable[2] == "esc_attr"){
				$val_instance1.="\n\r"."		\$instance['$exp_variable[1]'] = strip_tags( \$new_instance['$exp_variable[1]']);";
				$selection1 .=  "\n\r"."		$$exp_variable[1] = apply_filters( 'widget_$exp_variable[1]', empty(\$instance['$exp_variable[1]'] ) ? __('', 'genthemesv1') : \$instance['$exp_variable[1]'], \$instance, \$this->id_base);";
		}
		if($exp_variable[2] == "esc_html"){
				$val_instance2.="\n\r"."		\$instance['$exp_variable[1]'] = esc_html( \$new_instance['$exp_variable[1]']);";
				$selection2 .=  "\n\r"."		$$exp_variable[1] = apply_filters( 'widget_$exp_variable[1]', empty(\$instance['$exp_variable[1]'] ) ? __('', 'genthemesv1') : \$instance['$exp_variable[1]'], \$instance, \$this->id_base);";
		}
	    if($exp_variable[2] == "absint"){
				$val_instance3.="\n\r"."		\$instance['$exp_variable[1]'] = (int) \$new_instance['$exp_variable[1]'];";
				$selection3 .=  "\n\r"."		if ( ! isset(\$instance['$exp_variable[1]'] ) )\$instance['$exp_variable[1]'] = '$exp_variable[3]'; 
		if ( ! \$$exp_variable[1] = absint(\$instance['$exp_variable[1]'] ) )
		\$$exp_variable[1] = $exp_variable[3];";
		}
		
		
			
			$val_form.="\r"."		<p><label for=\"<?php echo esc_attr(\$this->get_field_id( '$exp_variable[1]' ) ); ?>\"><?php _e( '$exp_variable[1]:', 'genthemesv1'); ?></label></p>\n$field";
			$val_field.="\n\r"."		$$exp_variable[1] = isset( \$instance['$exp_variable[1]']) ? $exp_variable[2]( \$instance['$exp_variable[1]'] ) : $num;";
			
	
	}
    
    $model = file_get_contents('templates/widgets_html.php');
     
    
	$search_m = array('{slugwidget}',
					  '{content}', 
					  '{content_form}',
					  '{content_field}',
					  '{selection1}',
					  '{selection2}',
					  '{selection3}',
					  '{content_instance1}',
					  '{content_instance2}',
					  '{content_instance3}',
					  '{namewidget}'
					  );
	$replace_m = array($val,
					   $vals_wg,
					   $val_form,
					   $val_field,
					   $selection1,
					   $selection2,
					   $selection3,
					   $val_instance1,
					   $val_instance2,
					   $val_instance3,
					   str_replace('_', ' ', $val),
					  );
	
	$m_content = str_replace($search_m, $replace_m, $model);
	
	/* 2x replace different*/
	
	 return del($m_content,$val,$tes,$tesSearch);
	
	 
     //$file_wg = "wp/widgets/".$val.".php";
     //writefile($file_wg,$m_content); 
    
    /*//CREATE Widget */
}

function del($m_content,$val,$tes,$tesSearch){
	$tag_del ="del";
    $filename_del = str_replace('del','',$tag_del);
    preg_match_all("/<".$tag_del."[^>]*>(.*?)<\/$tag_del>/si", $m_content, $matches_del);
     
	foreach ($matches_del[0] as $key => $value_del) {
	 $exp_variable = explode(",", $value_del); 
 	 
	 if($exp_variable[4] == "img"){
	 	$val_rep .= ''.$value_del.'<img src="<?php echo $'.$exp_variable[1].'; ?>">'.',';
	 }
	 elseif($exp_variable[4] == "html"){
	 	$val_rep .= ''.$value_del.'<?php echo html_entity_decode($'.$exp_variable[1].'); ?>'.',';
	 }
	 else{
	 	$val_rep .= ''.$value_del.'<?php echo $'.$exp_variable[1].'; ?>'.',';
	 }
     $val_field.= ''.$value_del.','; 
      
    
    } 
    
    $ar = explode(",", $val_rep);
    $ar2 = explode(",", $val_field); 
    
    $search_m_2 = $ar2;
	$replace_m_2 = $ar;
    $m_content_2 = str_replace($search_m_2, $replace_m_2, $m_content);
	
     //$file_wg = "wp/widgets/".$val.".php";
     //writefile($file_wg,$m_content_2); 
    

    return delComplete($m_content_2,$val,$tes,$tesSearch);
}

function delComplete($m_content,$val,$tes,$tesSearch){
	$tag_del ="del"; 
    preg_match_all("/<".$tag_del."[^>]*>(.*?)<\/$tag_del>/si", $m_content, $matches_del);
     
	foreach ($matches_del[0] as $key => $value_del) {
		$d .= $value_del.'::';
    } 
     
    $ar = explode("::", $d);
    
    $search_m_2 = $ar;
	$replace_m_2 = array(''); 
    $m_content_2 = str_replace($search_m_2, $replace_m_2, $m_content);
	$content_file = str_replace("\r", "", $m_content_2);
	
	$search_m_3 = array("$tesSearch");
	$replace_m_3 = array('<?php echo $'.$tes.'; ?>');
	$content_file3 = str_replace($search_m_3, $replace_m_3, $content_file);
	 
    $file_wg = FolderName::get('themeFolder')."/widgets/".$val.".php";
    writefile($file_wg,$content_file3); 
}

function strSlug($val){  
	$exp_variable = str_replace(" ","-",$val); 
	return strtolower($exp_variable);
}
function replaceString($val){ 
	$exp_variable = explode(":", $val); 
	return $exp_variable[0];
}
function writefile($file,$content){ 
	$ourFileHandle = fopen($file, 'w') or die("can't open file");
	fwrite($ourFileHandle,$content);
	fclose($ourFileHandle);
}
 
	
?>